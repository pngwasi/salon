<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

function reportInvoice()
{
    Route::get('report/invoice/entry/{id}', 'ReportController@invoiceHairdress')->name('report.invoice');
}

Route::get('/', fn () => redirect(route('login')));
Auth::routes(['register' => false]);

Route::prefix('search')
    ->name('search.')
    ->group(function () {
        Route::get('worker/', 'SearchController@worker')->name('worker');
        Route::get('subscriber/', 'SearchController@subscriber')->name('subscriber');
    });

Route::middleware(['auth:web'])
    ->group(function () {
        reportInvoice();
        Route::get('home/', 'HomeController@index')->name('home');
        Route::put('password/change', 'HomeController@changePassword')->name('change-password');

        Route::prefix('hairdress')
            ->name('hairdress')
            ->middleware('can:not-cashier')
            ->group(function () {
                Route::get('', 'HairdressController@home');
                Route::get('bonus/', 'HairdressController@bonus')->name('.bonus');
                Route::get('dept/', 'HairdressController@dept')->name('.dept');
                Route::get('salary/', 'HairdressController@salary')->name('.salary');
                Route::get('hairdressing/', 'HairdressController@hairdressing')->name('.hairdressing');
            });

        Route::namespace('Checkout')
            ->middleware('can:view-cashier')
            ->group(function () {
                Route::prefix('collect')
                    ->name('collect')
                    ->group(function () {
                        Route::get('', 'CollectController@home');
                        Route::get('entries/', 'CollectController@entries')->name('.entries');
                        Route::get('entries/search', 'CollectController@entriesSearch')->name('.entries.search');

                        Route::get('entries/profile/worker', 'CollectController@profileWorker')
                            ->name('.profile.worker')
                            ->withoutMiddleware(['auth:web', 'can:view-cashier']);
                        Route::get('entries/profile/subscriber', 'CollectController@profileSubcriber')
                            ->name('.profile.subscriber')
                            ->withoutMiddleware(['auth:web', 'can:view-cashier']);

                        Route::post('entries/new', 'CollectController@newEntry')->name('.profile.newEntry');

                        Route::get('bonus/', 'CollectController@bonus')->name('.bonus');
                        Route::get('bonus/add-bonus', 'CollectController@addBonus')->name('.bonus.add-bonus');
                        Route::post('bonus/add-bonus', 'CollectController@newBonus')->name('.bonus.newBonus');
                    });
                Route::prefix('disburse')
                    ->name('disburse')
                    ->group(function () {
                        Route::get('', 'DisburseController@home');
                        Route::get('sortie/', 'DisburseController@sortie')->name('.sortie');
                        Route::post('sortie/new', 'DisburseController@newSortie')->name('.newSortie');

                        Route::get('rejected/', 'DisburseController@rejected')->name('.rejected');
                        Route::get('unconfirmed/', 'DisburseController@unconfirmed')->name('.unconfirmed');
                        Route::get('search/', 'DisburseController@search')->name('.search');
                    });
            });

        Route::get('report/', 'HomeController@report')->name('report');
    });

Route::prefix('admin')->name('admin.')
    ->group(function () {
        Route::namespace('Admin\Auth')
            ->group(function () {
                Route::get('login/', "AdminLoginController@showLoginForm")->name('login');
                Route::post('login/', "AdminLoginController@login")->name('log-in');
            });
        Route::middleware(['auth.admin:admin'])
            ->group(function () {
                reportInvoice();
                Route::namespace('Admin')
                    ->group(function () {
                        Route::namespace('Auth')
                            ->group(function () {
                                Route::post('logout/', "AdminLoginController@logout")->name('logout');
                            });
                        Route::get('', "HomeController@index")->name('home');

                        Route::prefix('workers')
                            ->group(function () {
                                Route::get('', "AdminController@workers")->name('workers');
                                Route::post('', "AdminController@createWorker")->name('createWorker');
                                Route::get('cashier/', "AdminController@cashier")->name('cashier');
                                Route::put('modif/{id}/', "AdminController@modifyWorker")->name('modifyWorker');
                                Route::delete('del/{id}/', "AdminController@deleteWorkers")->name('deleteWorker');
                            });

                        Route::prefix('specifications')
                            ->group(function () {
                                Route::get('', "AdminController@specifications")->name('specifications');
                                Route::post('', "AdminController@newSpecification")->name('newSpecification');
                                Route::put('modif/{id}', "AdminController@modifySpecification")->name('modifySpecification');
                                Route::delete('del/{id}/', "AdminController@deleteSpecification")->name('deleteSpecification');

                                Route::get('paytype/', "AdminController@payType")->name('payType');
                                Route::post('newPayType/', "AdminController@newPayType")->name('newPayType');
                                Route::delete('payType/delete/{id}', "AdminController@deletePayType")->name('deletePayType');
                            });

                        Route::prefix('subsribers')
                            ->group(function () {
                                Route::get('', "SubscriberController@subscribers")->name('subscribers');
                                Route::post('', "SubscriberController@newSubscriber")->name('newSubscriber');

                                Route::put('modif/{id}/', "SubscriberController@modifysubsriber")->name('modifySubsriber');
                                Route::delete('del/{id}/', "SubscriberController@deletesubsribers")->name('deleteSubsriber');
                                Route::get('profile/', "SubscriberController@subsriberProfile")->name('subscriber.profile');

                                Route::get('subscriptions/{id?}', "SubscriberController@subscriptions")->name('subscriptions');
                                Route::post('subscription/', "SubscriberController@newSubscription")->name('newSubscription');
                                Route::put('subscription/modif/{id}', "SubscriberController@modifySubscription")->name('modifySubscription');
                                Route::delete('subscription/delete/{id}', "SubscriberController@deleteSubscription")->name('deleteSubscription');
                            });

                        Route::prefix('products')
                            ->group(function () {
                                Route::get('', "ProductsController@products")->name('products');
                                Route::post('', "ProductsController@newMaterial")->name('newMaterial');
                                Route::delete('del/{id}', "ProductsController@deleteProduct")->name('deleteProduct');
                                Route::put('modif/{id}', "ProductsController@modifyProduct")->name('modifyProduct');
                                Route::get('inventory/', "ProductsController@inventory")->name('products.inventory');
                            });

                        Route::prefix('disbursement')
                            ->name('disbursement')
                            ->group(function () {
                                Route::get('', "DisbursementController@disbursement");
                                Route::put('disbursement/approved/{id}', "DisbursementController@disburseApproved")->name('.approved');
                                Route::get('confirmed/', "DisbursementController@confirmed")->name('.confirmed');
                                Route::get('rejected/', "DisbursementController@rejected")->name('.rejected');
                                Route::get('request/', "DisbursementController@request")->name('.request');
                                Route::get('inventory/', "DisbursementController@inventory")->name('.inventory');
                            });

                        Route::prefix('collection')
                            ->name('collection.')
                            ->group(function () {
                                Route::get('', "CollectionController@hairdressing")->name('hairdressing');
                                Route::put('hairdressing/approved/{id}', "CollectionController@hairdressingApproved")->name('hairdressing.approved');

                                Route::get('bonus/', "CollectionController@bonus")->name('bonus');
                                Route::put('bonus/approved/{id}', "CollectionController@bonusApproved")->name('bonus.approved');
                            });
                        Route::prefix('user')
                            ->name('user.')
                            ->group(function () {
                                Route::get('profile/', "UserAdminController@profile")->name('profile');
                                Route::put('profile/', "UserAdminController@modifyProfile")->name('modify-profile');

                                Route::prefix('settings')
                                    ->middleware(['can:view-super'])
                                    ->name('settings')
                                    ->group(function () {
                                        Route::get('', "UserAdminController@settings");
                                        Route::put('admin/modify/{id}', "UserAdminController@modifyAdmin")->name('.admin.modify');
                                        Route::delete('admin/del/{id}', "UserAdminController@deleteAdmin")->name('.admin.delete');
                                        Route::post('admin/new/', "UserAdminController@newAdminstrateur")->name('.newAdmin');
                                    });
                            });
                        Route::prefix('salary')
                            ->name('salary')
                            ->group(function () {
                                Route::get('', "SalaryController@home");

                                Route::prefix('salaries')
                                    ->name('.salaries')
                                    ->group(function () {
                                        Route::get('', "SalaryController@salariesForHairdresser");
                                        Route::get('moderators/', "SalaryController@salariesForModerators")->name('.moderators');
                                        Route::put('moderators/approved/{id}', "SalaryController@salaryModeratorsApproved")->name('.moderators-approved');
                                        Route::post('moderators/newSalary', "SalaryController@salaryModeratorsNewSalary")->name('.moderators-newSalary');
                                    });

                                Route::prefix('advance')
                                    ->name('.advance')
                                    ->group(function () {
                                        Route::get('', "SalaryController@advanceForHairdresser");
                                        Route::get('moderators/', "SalaryController@advanceForModerators")->name('.moderators');
                                        Route::post('moderators/newAdvance', "SalaryController@newAdvanceForModerators")->name('.moderators-newAdvance');
                                    });

                                Route::prefix('dept')
                                    ->name('.dept')
                                    ->group(function () {
                                        Route::get('', "DeptController@home");
                                        Route::get('refunded/', "DeptController@refunded")->name('.refunded');
                                        Route::get('no-refunded/', "DeptController@dept")->name('.no-refunded');
                                        Route::put('approved/{id}', "DeptController@deptApproved")->name('.approved');
                                        Route::get('refunded/action/{id}', "DeptController@refundedAction")->name('.refunded-action');

                                        Route::post('newDept', "DeptController@newDept")->name('.newDept');
                                    });


                                Route::post('advance/newAdvance', "SalaryController@newAdvance")->name('.newAdvance');

                                Route::put('advance/approved/{id}', "SalaryController@advanceApproved")->name('.advance.approved-now-date');
                                Route::put('now-date/approved/{id}', "SalaryController@approvedAction")->name('.approved-now-date');
                            });

                        Route::prefix('report')
                            ->name('report')
                            ->group(function () {
                                Route::get('', "ReportController@home");
                                Route::get('generate/', "ReportController@generate")->name('.generate');
                                Route::get('generate/disburse/{id}', "ReportController@disburse")->name('.disburse');

                                Route::get('generate/salary/', "ReportController@salary")->name('.salary');
                                Route::get('generate/salary/moderator', "ReportController@salaryModerator")->name('.salary-moderator');
                            });
                    });
            });
    });
