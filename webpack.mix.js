const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('assets/js/app.js', 'public/assets/vendor/js')
    .sass('assets/sass/app.scss', 'public/assets/vendor/css')
    // .sass('assets/sass/dark/app.scss', 'public/assets/vendor/css/dark')
    .sourceMaps(false);

if (mix.inProduction()) {
    mix.version();
}

mix.disableNotifications();