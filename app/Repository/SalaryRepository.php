<?php

namespace App\Repository;

use App\Models\Format\ParamsQuery;
use Illuminate\Support\Facades\DB;

class SalaryRepository
{

    /**
     * 
     */
    public ParamsQuery $pm;

    /**
     * Undocumented function
     *
     * @param ParamsQuery $paramsQuery
     */
    public function __construct(ParamsQuery $paramsQuery)
    {
        $this->pm = $paramsQuery;
    }


    /**
     * @param HasMany|Builder $model
     * @return int
     */
    public function querySum($model)
    {
        return $model->where('approved', true)->sum('amount');
    }

    /**
     * @param HasMany|Builder $model
     * @return int
     */
    public function queryByNowDate($model)
    {
        return $this->querySum(
            $model->whereMonth('created_at', now()->format('m'))
                ->whereYear('created_at', now()->format('Y'))
        );
    }

    /**
     * Undocumented function
     *
     * @param HasMany|Builder $model
     * @param string $month
     * @param string $year
     * @return int
     */
    public function queryByYearMonth($model, $month, $year)
    {
        return $this->querySum(
            $model->whereMonth('created_at', $month)
                ->whereYear('created_at', $year)
        );
    }

    /**
     * Undocumented function
     *
     * @param int $amount
     * @return int
     */
    public function getByPourcentSalary($amount)
    {
        return round(($amount * 40 / 100), 2);
    }
    /**
     * Undocumented function
     *
     * @param \Illuminate\Database\Eloquent\Builder $model
     * @return mixed
     */
    public function performQuerySalaries($model)
    {
        $result = $model
            ->where('approved', true)
            ->select(DB::raw('user_id, YEAR(created_at) as year, MONTH(created_at) as month, SUM(amount) as sum_amount'))
            ->groupByRaw('MONTH(created_at)')
            ->groupByRaw('YEAR(created_at)')
            ->groupBy('user_id')
            ->latest()
            ->paginate(12);
        foreach ($result as $key => $v) {
            $user = $v->user;
            $result[$key]->_id = $user->id;
            $result[$key]->_name = $user->name;
            $result[$key]->_email = $user->email;
            $result[$key]->_phone = $user->phone;
            $result[$key]->_hairdress = $v->sum_amount;
            $result[$key]->_salary = $this->getByPourcentSalary($v->sum_amount);
            $result[$key]->_bonus = $this->queryByYearMonth($v->user->bonuses(), $v->month, $v->year);
            $result[$key]->_advance = $this->queryByYearMonth($v->user->salaryAdvance(), $v->month, $v->year);
            $result[$key]->_salaryStatus = $this->queryByYearMonth($v->user->salaries(), $v->month, $v->year);
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param \Illuminate\Database\Eloquent\Builder $model
     * @return mixed
     */
    public function performQuerySalariesForModerator($model)
    {
        $result = $model
            ->addSelect(DB::raw('*, YEAR(created_at) as year, MONTH(created_at) as month'))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('users')
                    ->whereRaw('salaries.user_id = users.id')
                    ->where(function ($query) {
                        $query->where('moderator', true)
                            ->orWhere('cashier', true);
                    });
            })
            ->latest()
            ->paginate(12);
        foreach ($result as $key => $v) {
            $result[$key]->_advance = $this->queryByYearMonth($v->user->salaryAdvance(), $v->month, $v->year);
        }
        return $result;
    }
}
