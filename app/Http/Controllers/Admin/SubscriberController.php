<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Format\ParamsQuery;
use App\Models\Subscriber;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class SubscriberController extends Controller
{

    public function __construct()
    {
    }

    /**
     * @return mixed
     */
    public function subscribers(Request $request, ParamsQuery $paramsQuery, Subscriber $subsciber)
    {
        $subscribers = $paramsQuery->search($request, $subsciber->newQuery())->paginate(10);
        return view('admin._subscribers.subscribers', compact('subscribers'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function newSubscriber(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:App\Models\Subscriber,email'],
            'address' => ['required', 'string', 'min:2'],
            'phone' => ['required', 'string', 'regex:/^(\+\d{12}|\d{11}|\+\d{2}-\d{3}-\d{7})$/', 'unique:App\Models\Subscriber,phone']
        ]);
        Subscriber::create([
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone
        ]);
        return ['message' => 'Engregistré'];
    }

    /**
     * @param int $id
     * @param Subscriber $subsciber
     * @param Request $request
     * @return array
     */
    public function modifysubsriber($id, Subscriber $subsciber, Request $request)
    {
        $user = $subsciber->newQuery()->findOrFail($id);
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('subscribers', 'email')->ignore($user)],
            'address' => ['required', 'string', 'min:2'],
            'phone' => ['required', 'string', 'regex:/^(\+\d{12}|\d{11}|\+\d{2}-\d{3}-\d{7})$/', Rule::unique('subscribers', 'phone')->ignore($user)]
        ]);
        $filled = $user->fill([
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone
        ]);
        $filled->save();
        return ['message' => trans('Enregistré')];
    }
    /**
     * @param int $id
     * @param Subscriber $u
     * @param Request $req
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function deletesubsribers($id, Subscriber $u, Request  $req)
    {
        $user = $u->newQuery()->findOrFail($id);
        $user->active ? ($user->active = false) : ($user->active = true);
        $user->save();
        return redirect($req->query('redirect'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function newSubscription(Request $request, Subscriber $sb)
    {
        $request->validate([
            'email_phone' => ['required', 'string', 'max:255'],
            'date_entry' => ['required', 'date', 'max:255'],
            'date_expire' => ['required', 'date', 'max:255', 'after:date_entry'],
            'montant' => ['required', 'numeric', 'min:0.1'],
            'per' => ['required', 'numeric', 'min:0.1'],
        ]);
        $subsciber = $sb->newQuery()->where('email', $request->email_phone)->orWhere('phone', $request->email_phone)->first();
        abort_if(!$subsciber, 422, 'Email ou Tel incorrect');
        $subsciber->subscriptions()->create([
            'start' => $request->date_entry,
            'end' => $request->date_expire,
            'amount' => $request->montant,
            'per' => $request->per
        ]);
        return ['message' => trans('Enregistré')];
    }

    /**
     * @param int $id
     * @param Subscription $subscription
     * @param Request $request
     * @return array
     */
    public function modifySubscription($id, Subscription $subscription, Request $request)
    {
        $sb = $subscription->newQuery()->findOrFail($id);
        $request->validate([
            'start' => ['required', 'date', 'max:255'],
            'end' => ['required', 'date', 'max:255', 'after:start'],
            'amount' => ['required', 'numeric', 'min:0.1'],
            'per' => ['required', 'numeric', 'min:0.1'],
        ]);
        $sb->fill([
            'start' => $request->start,
            'end' => $request->end,
            'amount' => $request->amount,
            'per' => $request->per
        ])->save();
        return ['message' => trans('Enregistré')];
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function subscriptions(Request $request, Subscriber $sb)
    {
        $subscriptions = [];
        $subscriber = null;
        if (!is_null($id = $request->query('id'))) {
            $subscriber = $sb->newQuery()->find($id);
            if ($subscriber) {
                $subscriptions = $subscriber->subscriptions()->latest()->get();
            }
        }
        return view('admin._subscribers.subscriptions', compact('subscriptions', 'subscriber'));
    }

    /**
     * @param int $id
     * @param Subscription $u
     * @param Request $req
     * @return void
     */
    public function deleteSubscription($id, Subscription $u, Request  $req)
    {
        $sb = $u->newQuery()->findOrFail($id);
        $sb->active ? ($sb->active = false) : ($sb->active = true);
        $sb->save();
        return redirect($req->query('redirect'));
    }

    /**
     * @return mixed
     */
    public function subsriberProfile()
    {
        return view('admin._subscribers.profile');
    }
}
