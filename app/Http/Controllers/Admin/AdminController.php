<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Format\ParamsQuery;
use App\Models\PayType;
use App\Models\Specification;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class AdminController extends Controller
{

    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @param ParamsQuery $paramsQuery
     * @param User $user
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function workers(Request $request, ParamsQuery $paramsQuery, User $user)
    {
        $users = $paramsQuery->search($request, $user->newQuery())->paginate(10);
        return view('admin._workers.workers', compact('users'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cashier(Request $request, User $user)
    {
        $query = $user->newQuery()->where('cashier', true)
            ->orWhere('moderator', true);
        $users = $query->paginate(10);
        return view('admin._workers.cashier', compact('users'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function createWorker(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'address' => ['required', 'string', 'min:2'],
            'phone' => ['required', 'string', 'regex:/^(\+\d{12}|\d{11}|\+\d{2}-\d{3}-\d{7})$/', 'unique:users']
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'address' => $request->address,
            'phone' => $request->phone,
            'moderator' => ($request->moderator ? true : false),
            'cashier' => ($request->cashier ? true : false),
        ]);
        return ['message' => trans('Enregistré')];
    }

    /**
     * @param int $id
     * @param User $user
     * @param Request $request
     * @return array
     */
    public function modifyWorker($id, User $user, Request $request)
    {
        $user = $user->newQuery()->findOrFail($id);
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->ignore($user)],
            'password' => ['nullable', 'string', 'min:6', 'confirmed'],
            'address' => ['required', 'string', 'min:2'],
            'phone' => ['required', 'string', 'regex:/^(\+\d{12}|\d{11}|\+\d{2}-\d{3}-\d{7})$/', Rule::unique('users', 'phone')->ignore($user)]
        ]);
        $fillable = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'phone' => $request->phone,
            'moderator' => ($request->moderator ? true : false),
            'cashier' => ($request->cashier ? true : false),
        ];
        if ($request->filled('password')) {
            $fillable['password'] =  Hash::make($request->password);
        }
        $filled = $user->fill($fillable);
        $filled->save();
        return ['message' => trans('Enregistré')];
    }

    /**
     * @param int $id
     * @param User $u
     * @param Request $req
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteWorkers($id, User $u, Request  $req)
    {
        $user = $u->newQuery()->findOrFail($id);
        $user->active ? ($user->active = false) : ($user->active = true);
        $user->save();
        return redirect($req->query('redirect'));
    }

    /**
     * @param Specification $sp
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function specifications(Specification $sp)
    {
        $specifications = $sp->newQuery()->latest()->paginate(10);
        return view('admin._specifications.specifications', compact('specifications'));
    }

    /**
     * @param PayType $payType
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function payType(PayType $payType)
    {
        $pay_types = $payType->newQuery()->latest()->get();
        return view('admin._specifications.pay-type', compact('pay_types'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function newSpecification(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:225'],
            'montant' => ['required', 'numeric', 'min:0.1'],
            'tax' => ['nullable', 'numeric']
        ]);
        Specification::create([
            'name' => $request->name,
            'amount' => $request->montant,
            'tax' => $request->tax ?: 0
        ]);
        return ['message' => trans('Enregistré')];
    }

    /**
     * @param int $id
     * @param Specification $specification
     * @param Request $request
     * @return void
     */
    public function modifySpecification($id, Specification $specification, Request $request)
    {
        $sp = $specification->newQuery()->findOrFail($id);
        $request->validate([
            'name' => ['required', 'string', 'max:225'],
            'amount' => ['required', 'numeric', 'min:0.1'],
            'tax' => ['nullable', 'numeric']
        ]);
        $sp->fill([
            'name' => $request->name,
            'amount' => $request->amount,
            'tax' => $request->tax ?: 0
        ])->save();
        return ['message' => trans('Enregistré')];
    }

    /**
     * @param int $id
     * @param Specification $u
     * @param Request $req
     * @return array
     */
    public function deleteSpecification($id, Specification $u, Request  $req)
    {
        $sp = $u->newQuery()->findOrFail($id);
        $sp->active ? ($sp->active = false) : ($sp->active = true);
        $sp->save();
        return redirect($req->query('redirect'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function newPayType(Request $request)
    {
        $request->validate([
            'type_name' => ['required', 'string', 'max:255', 'unique:App\Models\PayType,name']
        ]);
        $pay_types = PayType::count();
        if ($pay_types >= 5) {
            abort(422, 'Liste Pleine');
        }
        PayType::create([
            'name' => $request->type_name
        ]);
        return ['message' => trans('Engreistré')];
    }

    /**
     * @param int $id
     * @param PayType $payType
     * @param Request $req
     * @return mixed
     */
    public function deletePayType($id, PayType $payType, Request $req) {
        $y = $payType->newQuery()->findOrFail($id);
        $y->delete();
        return redirect($req->query('redirect'));
    }
}
