<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Disbursement;
use App\Models\Format\ParamsQuery;
use App\Models\Salary;
use App\Report\ReportTable;
use App\Repository\SalaryRepository;
use App\User;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $report;


    public function __construct(ReportTable $report)
    {
        $this->report = $report;
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function home()
    {
        return view('admin.report');
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function generate(Request $request)
    {
        $keys = collect($request->all())
            ->filter(fn ($v) => !!$v)
            ->keys()
            ->all();
        return $this->report->generate($keys);
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @param Disbursement $disbursement
     * @return \Illuminate\Http\Response
     */
    public function disburse($id, Disbursement $disbursement)
    {
        $model = $disbursement->newQuery()->findOrFail($id);
        return $this->report->disburse($model);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param Salary $sal
     * @param ParamsQuery $pm
     * @param SalaryRepository $sp
     * @return \Illuminate\Http\Response
     */
    public function salaryModerator(Request $request, Salary $sal, ParamsQuery $pm, SalaryRepository $sp)
    {
        $id = $request->query('id');
        $year = $request->query('year');
        $month = $request->query('month');
        $salary = $sal->newQuery()->findOrFail($id);
        $salary->_advance = $sp->queryByYearMonth($salary->user->salaryAdvance(), $month, $year);
        $salary->year = $year;
        $salary->month = $month;
        return $this->report->salaryModerator($salary);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param User $user
     * @param ParamsQuery $pm
     * @param SalaryRepository $sp
     * @return \Illuminate\Http\Response
     */
    public function salary(Request $request, User $user, ParamsQuery $pm, SalaryRepository $sp)
    {
        $uid = $request->query('uid');
        $u = $user->newQuery()->findOrFail($uid);
        $users = $sp->performQuerySalaries(
            $pm->queryByDateParams($request, $u->hairdress(), 12, false)
        );
        return $this->report->salary(count($users->items()) ? $users->items()[0] : null);
    }
}
