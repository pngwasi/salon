<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bonus;
use App\Models\Format\ParamsQuery;
use App\Models\Hairdressing;
use App\User;
use Illuminate\Http\Request;

class CollectionController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @param Request $request
     * @param Hairdressing $hairdressing
     * @param ParamsQuery $pm
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function hairdressing(Request $request, User $user, Hairdressing $hairdressing, ParamsQuery $pm)
    {
        $uid = $request->query('uid');
        if ($uid && $u = $user->newQuery()->find($uid)) {
            $collect = $pm->queryByDateParams($request, $u->hairdress());
        } else {
            $collect = $pm->queryByDateParams($request, $hairdressing->newQuery());
        }
        $us = $u ?? null;
        return view('admin._collection.hairdressing', compact('collect', 'us'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param Hairdressing $model
     * @return mixed
     */
    public function hairdressingApproved($id, Request $request, Hairdressing $model)
    {
        $model = $model->newQuery()->findOrFail($id);
        $query = $request->query('status');
        if (is_numeric($query)) {
            $model->approved = boolval($query);
            $model->rejected = !boolval($query);
            $model->save();
        }
        return redirect($request->query('redirect'));
    }

    /**
     * @param Request $request
     * @param User $user
     * @param Bonus $bonus
     * @param ParamsQuery $pm
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function bonus(Request $request, User $user, Bonus $bonus, ParamsQuery $pm)
    {
        $uid = $request->query('uid');
        if ($uid && $u = $user->newQuery()->find($uid)) {
            $bonuses = $pm->queryByDateParams($request, $u->bonuses());
        } else {
            $bonuses = $pm->queryByDateParams($request, $bonus->newQuery());
        }
        $us = $u ?? null;
        return view('admin._collection.bonus', compact('bonuses', 'us'));
    }

    /**
     * @param Request $request
     * @param Bonus $bonus
     * @return mixed
     */
    public function bonusApproved($id, Request $request, Bonus $bonus)
    {
        $bonus = $bonus->newQuery()->findOrFail($id);
        $query = $request->query('status');
        if (is_numeric($query)) {
            $bonus->approved = boolval($query);
            $bonus->rejected = !boolval($query);
            $bonus->save();
        }
        return redirect($request->query('redirect'));
    }
}
