<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dept;
use App\User;
use Illuminate\Http\Request;

class DeptController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function home(Dept $dept, Request $request, User $user)
    {
        $uid = $request->query('uid');
        if ($uid && $u = $user->newQuery()->find($uid)) {
            $depts = $u->depts()->latest()->paginate(12);
        } else {
            $depts = $dept->newQuery()->latest()->paginate(12);
        }
        $us = $u ?? null;
        return view('admin._salary._dept.home', compact('us', 'depts'));
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function refunded(Dept $dept)
    {
        $depts = $dept->newQuery()->where('refunded', true)->latest()->paginate(12);
        return view('admin._salary._dept.refunded', compact('depts'));
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function dept(Dept $dept)
    {
        $depts = $dept->newQuery()->where('refunded', false)->latest()->paginate(12);
        return view('admin._salary._dept.dept', compact('depts'));
    }

    /**
     * @param [type] $id
     * @param Request $request
     * @param Dept $model
     * @return void
     */
    public function deptApproved($id, Request $request, Dept $model)
    {
        $model = $model->newQuery()->findOrFail($id);
        $query = $request->query('status');
        if (is_numeric($query)) {
            $model->approved = boolval($query);
            $model->rejected = !boolval($query);
            $model->save();
        }
        return redirect($request->query('redirect'));
    }

    /**
     * @param [type] $id
     * @param Request $request
     * @param Dept $model
     * @return void
     */
    public function refundedAction($id, Request $request, Dept $model)
    {
        $model = $model->newQuery()->findOrFail($id);
        $model->refunded = !$model->refunded;
        $model->save();
        return redirect($request->query('redirect'));
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function newDept(Request $request, User $u)
    {
        $request->validate([
            'uid' => ['required', 'string', 'exists:App\User,id'],
            'montant' => ['required', 'numeric', 'min:0.1']
        ]);
        $user = $u->newQuery()->find($request->uid);
        $user->depts()->create([
            'amount' => $request->montant
        ]);
        return ['message' => trans('Enregistré')];
    }
}
