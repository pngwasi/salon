<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bonus;
use App\Models\Disbursement;
use App\Models\Format\Chart;
use App\Models\Hairdressing;
use App\Models\LoginActivity;
use App\Models\Material;
use App\Models\Subscriber;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(
        Hairdressing $hr,
        Disbursement $ds,
        Bonus $bs,
        User $us,
        Subscriber $sb,
        Material $ms,
        LoginActivity $la,
        Chart $chart
    ) {
        $chart_collect = $chart->queryChart($hr->newQuery(), 12);
        $chart_disburse = $chart->queryChart($ds->newQuery(), 12);
        $user = $us->newQuery()->where('active', true)->count();
        $subcriber = $sb->newQuery()->where('active', true)->count();
        $material = $ms->newQuery()->where('status', true)->count();
        $r = $hr->newQuery()->where('approved', true)->sum('amount');
        $bonus = $r > 0 ? round(($bs->newQuery()->where('approved', true)->sum('amount') * 100) / $r) : 0;
        $activities = $la->newQuery()->whereHasMorph('logable', User::class)->limit(5)->latest()->get();
        return view('admin.home', compact('chart_collect', 'chart_disburse', 'user', 'subcriber', 'material', 'bonus', 'activities'));
    }
}
