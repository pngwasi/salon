<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Disbursement;
use App\Models\Format\ParamsQuery;
use Illuminate\Http\Request;

class DisbursementController extends Controller
{
    private $disburse;


    public function __construct(Disbursement $disburse)
    {
        $this->disburse = $disburse;
    }

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function disbursement(Request $request, ParamsQuery $paramsQuery)
    {
        $disbursements = $paramsQuery->queryByDateParams($request, $this->disburse->newQuery());
        return view('admin._disbursement.disbursement', compact('disbursements'));
    }

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function confirmed()
    {
        $disbursements = $this->disburse->newQuery()->where('approved', true)->latest()->paginate(10);
        return view('admin._disbursement.confirmed', compact('disbursements'));
    }

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function rejected()
    {
        $disbursements = $this->disburse->newQuery()->where('rejected', true)->latest()->paginate(10);
        return view('admin._disbursement.rejected', compact('disbursements'));
    }

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function request()
    {
        $disbursements = $this->disburse->newQuery()
            ->where('rejected', false)
            ->where('approved', false)
            ->latest()
            ->paginate(10);
        return view('admin._disbursement.request', compact('disbursements'));
    }
    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function inventory()
    {
        return view('admin._disbursement.inventory');
    }

    /**
     * @param int $id
     * @param Request $request
     * @return mixed
     */
    public function disburseApproved($id, Request $request)
    {
        $model = $this->disburse->newQuery()->findOrFail($id);
        $query = $request->query('status');
        if (is_numeric($query)) {
            $model->approved = boolval($query);
            $model->rejected = !boolval($query);
            $model->save();
        }
        return redirect($request->query('redirect'));
    }
}
