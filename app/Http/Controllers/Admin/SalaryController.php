<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Format\ParamsQuery;
use App\Models\Hairdressing;
use App\Models\Salary;
use App\Models\salaryAdvance;
use App\Repository\SalaryRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalaryController extends Controller
{

    /**
     * Undocumented variable
     *
     * @var SalaryRepository
     */
    private $sp;


    public function __construct(SalaryRepository $sp)
    {
        $this->sp = $sp;
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function home(User $user)
    {
        $users = $user->newQuery()->where('active', true)->paginate(10);
        foreach ($users as $key => $user) {
            $bonus = $this->sp->queryByNowDate($user->bonuses());
            $hairdress = $this->sp->queryByNowDate($user->hairdress());
            $advance = $this->sp->queryByNowDate($user->salaryAdvance());
            $users[$key]->_bonus = $bonus;
            $users[$key]->_hairdress = $hairdress;
            $users[$key]->_salary = $this->sp->getByPourcentSalary($hairdress);
            $users[$key]->_advance = $advance;
            $users[$key]->_salaryStatus = $this->sp->queryByNowDate($user->salaries());
        }
        return view('admin._salary.home', compact('users'));
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function salariesForHairdresser(Request $request, User $user, Hairdressing $hairdressing, ParamsQuery $pm)
    {
        $uid = $request->query('uid');
        if ($uid && $u = $user->newQuery()->find($uid)) {
            $users = $this->sp->performQuerySalaries(
                $pm->queryByDateParams($request, $u->hairdress(), 12, false)
            );
        } else {
            $users = $this->sp->performQuerySalaries(
                $pm->queryByDateParams($request, $hairdressing->newQuery(), 12, false)
            );
        }
        $us = $u ?? null;
        return view('admin._salary._salaries.hairdresser', compact('users', 'us'));
    }


    /**
     * Undocumented function
     *
     * @param Salary $salary
     * @return void
     */
    public function salariesForModerators(Salary $salary, Request $request, ParamsQuery $pm, User $user)
    {
        $uid = $request->query('uid');
        if ($uid && $u = $user->newQuery()->find($uid)) {
            $salaries = $this->sp->performQuerySalariesForModerator(
                $pm->queryByDateParams($request, $u->salaries(), 12, false)
            );
        } else {
            $salaries = $this->sp->performQuerySalariesForModerator(
                $pm->queryByDateParams($request, $salary->newQuery(), 12, false)
            );
        }
        $us = $u ?? null;
        return view('admin._salary._salaries.moderator', compact('salaries', 'us'));
    }

    /**
     * Undocumented function
     *
     * @param [type] $id
     * @param Request $request
     * @param Salary $model
     * @return mixed
     */
    public function salaryModeratorsApproved($id, Request $request, Salary $model)
    {
        $model = $model->newQuery()->findOrFail($id);
        $query = $request->query('status');
        if (is_numeric($query)) {
            $model->approved = boolval($query);
            $model->rejected = !boolval($query);
            $model->save();
        }
        return redirect($request->query('redirect'));
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param User $u
     * @return mixed
     */
    public function salaryModeratorsNewSalary(Request $request, User $u)
    {
        $request->validate([
            'uid' => ['required', 'string', 'exists:App\User,id'],
            'montant' => ['required', 'numeric', 'min:0.1']
        ]);
        $user = $u->newQuery()->find($request->uid);
        abort_if((!$user->moderator && !$user->cashier), 400, trans("Veuillez entrer un bon utilisateur (Cassier ou moderateur)"));
        $user->salaries()
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->updateOrCreate([], [
                'amount' => $request->montant,
            ]);
        return ['message' => trans('Enregistré')];
    }
    /**
     * Undocumented function
     *
     * @param int $id
     * @param User $user
     * @return mixed
     */
    public function approvedAction($id, User $model, Request $request)
    {
        $user = $model->newQuery()->findOrFail($id);
        $query = $request->query('status');
        if (is_numeric($query)) {
            if (boolval($query)) {
                $hairdress = $this->sp->queryByNowDate($user->hairdress());
                $user->salaries()->firstOrCreate([
                    'amount' => $hairdress,
                    'expected' => $hairdress,
                    'approved' => true,
                    'rejected' => false
                ]);
            } else {
                $user->salaries()
                    ->whereMonth('created_at', now()->format('m'))
                    ->whereYear('created_at', now()->format('Y'))->delete();
            }
        }
        return redirect($request->query('redirect'));
    }

    /**
     * Undocumented function
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function advanceForHairdresser(salaryAdvance $as)
    {
        $advances = $as->newQuery()
            ->addSelect(DB::raw('*, YEAR(created_at) as year, MONTH(created_at) as month'))
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->latest()
            ->paginate(12);
        foreach ($advances as $key => $ad) {
            $s = $this->sp->queryByNowDate($ad->user->hairdress());
            $advances[$key]->salary_status = $this->sp->getByPourcentSalary($s);
        }
        return view('admin._salary._advance.hairdresser', compact('advances'));
    }

    /**
     * Undocumented function
     *
     * @param salaryAdvance $as
     * @return mixed
     */
    public function advanceForModerators(salaryAdvance $as)
    {
        $advances = $as->newQuery()
            ->with('user')
            ->addSelect(DB::raw('*, YEAR(created_at) as year, MONTH(created_at) as month'))
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->whereExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('users')
                    ->whereRaw('salary_advances.user_id = users.id')
                    ->where(function ($query) {
                        $query->where('moderator', true)
                            ->orWhere('cashier', true);
                    });
            })
            ->latest()
            ->simplePaginate(12);
        return view('admin._salary._advance.moderator', compact('advances'));
    }



    /**
     * Undocumented function
     *
     * @param Request $request
     * @return mixed
     */
    public function newAdvanceForModerators(Request $request, User $u)
    {
        $request->validate([
            'uid' => ['required', 'string', 'exists:App\User,id'],
            'montant' => ['required', 'numeric', 'min:0.1']
        ]);
        $user = $u->newQuery()->find($request->uid);
        abort_if((!$user->moderator && !$user->cashier), 400, trans("Veuillez entrer un bon utilisateur (Cassier ou moderateur)"));
        $user->salaryAdvance()
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->updateOrCreate([], [
                'amount' => $request->montant,
            ]);
        return ['message' => trans('Enregistré')];
    }
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return mixed
     */
    public function newAdvance(Request $request, User $u)
    {
        $request->validate([
            'uid' => ['required', 'string', 'exists:App\User,id'],
            'montant' => ['required', 'numeric', 'min:0.1']
        ]);
        $user = $u->newQuery()->find($request->uid);
        $salary = $this->sp->getByPourcentSalary($this->sp->queryByNowDate($user->hairdress()));
        $amount = doubleval($request->montant);
        abort_if(($amount > $salary), 400, "Le montant avance est superieure au salaire disponible ($ {$salary} )");
        $user->salaryAdvance()
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))
            ->updateOrCreate([], [
                'amount' => $amount,
                'salary' => $salary
            ]);
        return ['message' => trans('Enregistré')];
    }

    /**
     * @param int $id
     * @param Request $request
     * @param Hairdressing $model
     * @return mixed
     */
    public function advanceApproved($id, Request $request, salaryAdvance $model)
    {
        $model = $model->newQuery()->findOrFail($id);
        $query = $request->query('status');
        if (is_numeric($query)) {
            $model->approved = boolval($query);
            $model->rejected = !boolval($query);
            $model->save();
        }
        return redirect($request->query('redirect'));
    }
}
