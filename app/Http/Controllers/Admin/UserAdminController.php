<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserAdminController extends Controller
{
    private Admin $admin;

    /**
     * @param Admin $admin
     */
    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }


    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function profile()
    {
        return view('admin._user-admin.profile');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function modifyProfile(Request $request)
    {
        $user = $request->user('admin');
        $request->validate([
            'email' => ['required', 'email', 'string', Rule::unique('admins', 'email')->ignore($user)],
            'password' => ['nullable', 'string', 'min:6', 'confirmed']
        ]);
        $user->fill([
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ])->save();
        return ['message' => trans('Enregistré')];
    }

    /**
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function settings()
    {
        $supers = $this->admin->newQuery()->where('super', true)->get();
        $admins = $this->admin->newQuery()->where('super', false)->latest()->get();
        return view('admin._user-admin.settings', compact('supers', 'admins'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @return array
     */
    public function modifyAdmin($id, Request $request)
    {
        $user = $this->admin->newQuery()->findOrFail($id);
        $request->validate([
            'email' => ['required', 'email', Rule::unique('admins', 'email')->ignore($user)],
            'password' => ['nullable', 'string', 'min:6', 'confirmed']
        ]);
        $filled = $user->fill([
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        $filled->save();
        return ['message' => trans('Enregistré')];
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function newAdminstrateur(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'unique:App\Models\Admin,email'],
            'password' => ['required', 'string', 'min:6', 'confirmed']
        ]);
        
        $this->admin->newQuery()->create([
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);
        return ['message' => trans('Enregistré')];
    }


    /**
     * @param int $id
     * @param Request $req
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function deleteAdmin($id, Request $req)
    {
        $user = $this->admin->newQuery()->findOrFail($id);
        $user->active ? ($user->active = false) : ($user->active = true);
        $user->save();
        return redirect($req->query('redirect'));
    }
}
