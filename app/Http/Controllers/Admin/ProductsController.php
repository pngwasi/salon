<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\MaterialStock;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProductsController extends Controller
{
    public function __construct()
    {
    }

    public function products(MaterialStock $materials)
    {
        $stock = $materials->newQuery()
            ->with('material')
            ->select('material_id', DB::raw('DATE(created_at) as date'), DB::raw('COUNT(id) as total_stock'))
            ->groupByRaw('DATE(created_at)')
            ->groupBy('material_id')
            ->latest()
            ->get();
        return view('admin._products.products', compact('stock'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function newMaterial(Request $request, Material $material)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:225', 'unique:App\Models\Material'],
            'price' => ['required', 'numeric', 'min:0.1'],
            'stock' => ['nullable', 'numeric']
        ]);
        $material = $material->newQuery()->create([
            'name' => $request->name,
            'price' => $request->price,
        ]);
        for ($i = 0; $i < abs(intval($request->stock ?: 0)); $i++) {
            $material->stock()->create(['status' => true]);
        }
        return ['message' => trans('Engreistré')];
    }

    /**
     * @param int $id
     * @param Request $request
     * @param Material $m
     * @return array
     */
    public function modifyProduct($id, Request $request, Material $m)
    {
        $material = $m->newQuery()->findOrFail($id);
        $request->validate([
            'name' => ['required', 'string', 'max:225', Rule::unique('materials', 'name')->ignore($material)],
            'price' => ['required', 'numeric', 'min:0.1'],
            'inactif_stock' => ['required', 'numeric', 'min:0'],
            'add_stock' => ['required', 'numeric', 'min:0'],
        ]);
        $material->stock()->limit(abs(intval($request->inactif_stock ?: 0)))
            ->get()
            ->each(function ($mt) {
                $mt->fill(['status' => false])->save();
            });
        for ($i = 0; $i < intval($request->add_stock ?: 0); $i++) {
            $material->stock()->create(['status' => true]);
        }
        $material->fill([
            'name' => $request->name,
            'price' => $request->price,
        ])->save();
        return ['message' => trans('Engreistré')];
    }

    /**
     * @param Material $m
     * @return array
     */
    public function inventory(Material $m)
    {
        $materials = $m->newQuery()->with('stock')->latest()->paginate(8);
        foreach ($materials as $material) {
            $material->activeStock = $material->stock()->where('status', true)->count();
        }
        return view('admin._products.inventory', compact('materials'));
    }

    /**
     * @param int $id
     * @param Request $req
     * @param Material $u
     * @return array
     */
    public function deleteProduct($id, Request $req, Material $u)
    {
        $user = $u->newQuery()->findOrFail($id);
        $user->status ? ($user->status = false) : ($user->status = true);
        $user->save();
        return redirect($req->query('redirect'));
    }
}
