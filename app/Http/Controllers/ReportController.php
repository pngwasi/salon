<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Hairdressing;
use App\Report\ReportTable;
use Illuminate\Http\Request;

class ReportController extends Controller
{   

    private ReportTable $report;

    public function __construct(ReportTable $report)
    {
        $this->report = $report;
    }

    /**
     * @param int $id
     * @param Hairdressing $collection
     * @return \Illuminate\Http\Response
     */
    public function invoiceHairdress($id, Hairdressing $collection) {
        $hairdress = $collection->newQuery()->findOrFail($id);
        return $this->report->invoiceHairdress($hairdress);
    }

}
