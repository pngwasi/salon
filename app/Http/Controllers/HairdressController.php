<?php

namespace App\Http\Controllers;

use App\Models\Format\Chart;
use App\Models\Format\ParamsQuery;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HairdressController extends Controller
{

    private ParamsQuery $paramsQuery;

    /**
     * @param ParamsQuery $paramsQuery
     */
    public function __construct(ParamsQuery $paramsQuery)
    {
        $this->paramsQuery = $paramsQuery;
    }

    /**
     * @param HasMany|Builder $model
     * @return int
     */
    private function querySum($model)
    {
        return $model->where('approved', true)->sum('amount');
    }

    /**
     * @param HasMany|Builder $model
     * @return void
     */
    private function queryByNowDate($model)
    {
        return $this->querySum(
            $model->whereMonth('created_at', now()->format('m'))
                ->whereYear('created_at', now()->format('Y'))
        );
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home(Request $request, Chart $chart)
    {
        $user = $request->user('web');
        $bonus = $this->queryByNowDate($user->bonuses());
        $hairdress = $this->queryByNowDate($user->hairdress());
        $salary = round(($hairdress * 40 / 100), 2);
        $depts = $this->querySum($user->depts());
        $chart_hairdress = $chart->queryChart($user->hairdress());
        $chart_bonus = $chart->queryChart($user->bonuses());
        return view('_hairdress.home', compact('bonus', 'salary', 'hairdress', 'depts', 'chart_hairdress', 'chart_bonus'));
    }

    /**
     * @param Request $request
     * @param HasMany $model
     */
    private function queryByDate(Request $request, HasMany $model)
    {
        $model = $model->addSelect(DB::raw('*, YEAR(created_at) as year, MONTHNAME(created_at) as month'));
        return $this->paramsQuery->queryByDateParams($request, $model);
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function bonus(Request $request)
    {
        $user = $request->user('web');
        $bonus = $this->queryByDate($request, $user->bonuses());
        return view('_hairdress.bonus', compact('bonus'));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dept(Request $request)
    {
        $user = $request->user('web');
        $depts = $this->queryByDate($request, $user->depts());
        return view('_hairdress.dept', compact('depts'));
    }

    /**
     * Undocumented function
     *
     * @param HasMany|Builder $model
     * @param string $month
     * @param string $year
     * @return mixed
     */
    public function queryByYearMonth($model, $month, $year)
    {
        return $this->querySum(
            $model->whereMonth('created_at', $month)
                ->whereYear('created_at', $year)
        );
    }

    /**
     * @param \Illuminate\Database\Eloquent\Collection|static[] $salaries
     * @return mixed
     */
    private function getSalary($salaries)
    {
        return $salaries->groupBy('month')->map(function ($v, $k) {
            $ex = $v[0];
            $amount = collect($v)->sum(fn ($r) => $r->amount);
            $m = round(($amount * 40 / 100), 2);
            return [
                'id' => '#',
                'amount' => $m,
                'month' => $k,
                'expected' => $m,
                'entry_amount' => round($amount, 2),
                'year' => $ex->year,
                'approved' => true,
                'rejected' => false,
                'salaryStatus' => $this->queryByYearMonth($ex->user->salaries(), $ex->_month, $ex->year)
            ];
        })->slice(0, 12)->all();
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function salary(Request $request)
    {
        $user = $request->user('web');
        $model = $user->hairdress()->addSelect(DB::raw('*, YEAR(created_at) as year, MONTHNAME(created_at) as month, MONTH(created_at) as _month'));
        $salaries = $this->getSalary($this->paramsQuery->queryByDateParams($request, $model, 12, false)->latest()->get());
        return view('_hairdress.salary', compact('salaries'));
    }
    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function hairdressing(Request $request)
    {
        $user = $request->user('web');
        $hairdress = $this->queryByDate($request, $user->hairdress());
        return view('_hairdress.hairdressing', compact('hairdress'));
    }
}
