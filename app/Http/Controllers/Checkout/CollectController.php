<?php

namespace App\Http\Controllers\Checkout;

use App\Http\Controllers\Controller;
use App\Models\Bonus;
use App\Models\Format\Chart;
use App\Models\Hairdressing;
use App\Models\PayType;
use App\Models\Specification;
use App\Models\Subscriber;
use App\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Format\ParamsQuery;

class CollectController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home(Hairdressing $hr, Chart $chart)
    {
        $collect = $hr->newQuery()->latest()->paginate(10);
        $chart_collect = $chart->queryChart($hr->newQuery(), 12);
        return view('_collect.home', compact('collect', 'chart_collect'));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function entries(Specification $specification, PayType $payType)
    {
        $specifications = $specification->newQuery()->where('active', true)->latest()->get();
        $payTypes = $payType->newQuery()->latest()->get();
        return view('_collect._entries.home', compact('specifications', 'payTypes'));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function entriesSearch(Request $request, Hairdressing $hairdressing, ParamsQuery $pm)
    {
        $collect = $pm->queryByDateParams($request, $hairdressing->newQuery());
        return view('_collect._entries.search', compact('collect'));
    }

    /**
     * @param HasMany $query
     * @return mixed
     */
    public function queryByDate(HasMany $query)
    {
        return $query->where('approved', true)
            ->whereDay('created_at', now()->format('d'))
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function profileWorker(Request $request, User $user)
    {
        $id = trim($request->query('id'));
        $worker = $user->newQuery()->find($id);
        if (!$worker) {
            abort(400, 'Invalide requête');
            exit;
        }
        $hairdress = $this->queryByDate($worker->hairdress());
        $bonus = $this->queryByDate($worker->bonuses());
        $worker->count_hairdress = $hairdress->count();
        $worker->sum_hairdress = $hairdress->sum('amount');
        $worker->sum_bonus = $bonus->sum('amount');
        return $worker;
    }

    /**
     * @param HasMany $query
     * @return mixed
     */
    private function hasSubscription(HasMany $query)
    {
        return $query->where('active', true)
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'))->first();
    }

    /**
     * @param HasMany $query
     * @return mixed
     */
    private function subscriberHairdress(HasMany $query)
    {
        return $query->where('approved', true)
            ->whereMonth('created_at', now()->format('m'))
            ->whereYear('created_at', now()->format('Y'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function profileSubcriber(Request $request, Subscriber $sb)
    {
        $id = trim($request->query('id'));
        $subscriber =  $sb->newQuery()->find($id);
        if (!$subscriber) {
            abort(400, 'Invalide requête');
            exit;
        }
        $subscription = $this->hasSubscription($subscriber->subscriptions());
        $hairdress = $this->subscriberHairdress($subscriber->hairdress());;
        $amount = ($subscription ? $subscription->amount : 0);
        $per = ($subscription ? $subscription->per : 0);
        $subscriber->subscription_status = $subscription ? "Abonné, {$subscription->amount}" : 'Aucun abonnement actif';
        $subscriber->per_hairdress = $per;
        $subscriber->total_hairdress = $hairdress->count();
        $subscriber->rest_hairdress = $per > 0 ? abs((floor($amount / $per) - floor(($hairdress->sum('amount') ?: 0) / $per))) : 0;
        return $subscriber;
    }

    /**
     * @param Request $request
     * @return array
     */
    private function validateEntry(Request $request)
    {
        return $request->validate([
            'coiffeur' => [
                'required', 'numeric',
                Rule::exists('users', 'id')
                    ->where('email', $request->coiffeur_email)
                    ->where('active', true), 'min:1'
            ],
            'abonne' => [
                'nullable', 'numeric',
                Rule::exists('subscribers', 'id')
                    ->where('email', $request->abonne_email)
                    ->where('active', true), 'min:1'
            ],
            'specification' => [
                'nullable', 'numeric',
                Rule::exists('specifications', 'id')->where('active', true), 'min:1'
            ],
            'mode_pay' => [
                'nullable', 'numeric', Rule::exists('pay_types', 'id'), 'min:1'
            ],
            'montant' => ['required', 'numeric', 'min:0.1']
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function newEntry(Request $request, User $user, Subscriber $subscriber)
    {
        $this->validateEntry($request);
        $worker = $user->newQuery()->find($request->coiffeur);
        $has_subscriber = null;
        if ($request->has('has_subscriber') && $request->has_subscriber) {
            $has_subscriber = $subscriber->newQuery()->find($request->abonne);
            if (!$has_subscriber) abort(400, trans('Aucun abonné entré'));
            $has_subscription = $this->hasSubscription($has_subscriber->subscriptions());
            if (!$has_subscription) abort(400, trans('Aucun abonnement trouvé'));
        }
        $model = $worker->hairdress()->create([
            'amount' => $request->montant,
            'subscriber_id' => $has_subscriber ? $has_subscriber->id : null,
            'specification_id' => $request->specification,
            'pay_type_id' => $request->mode_pay
        ]);
        return ['message' => trans('Enregistré'), 'reportUrl' => route('report.invoice', ['id' => $model->id], false)];
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function bonus(Request $request, User $user, Bonus $bonus, ParamsQuery $pm)
    {
        $uid = $request->query('uid');
        if ($uid && $u = $user->newQuery()->find($uid)) {
            $bonuses = $pm->queryByDateParams($request, $u->bonuses());
        } else {
            $bonuses = $pm->queryByDateParams($request, $bonus->newQuery());
        }
        $us = $u ?? null;
        return view('_collect._bonus.search', compact('bonuses', 'us'));
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function addBonus()
    {
        return view('_collect._bonus.add-bonus');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function newBonus(Request $request, User $user)
    {
        $request->validate([
            'coiffeur' => [
                'required', 'numeric',
                Rule::exists('users', 'id')
                    ->where('active', true), 'min:1'
            ],
            'montant' => ['required', 'numeric', 'min:0.1']
        ]);
        $worker = $user->newQuery()->find($request->coiffeur);
        $worker->bonuses()->create([
            'amount' => $request->montant
        ]);
        return ['message' => trans('Enregistré')];
    }
}
