<?php

namespace App\Http\Controllers\Checkout;

use App\Http\Controllers\Controller;
use App\Models\Disbursement;
use App\Models\Format\Chart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Format\ParamsQuery;

class DisburseController extends Controller
{
    private Disbursement $disburse;

    /**
     * @param Disbursement $hr
     */
    public function __construct(Disbursement $hr)
    {
        $this->disburse = $hr;
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home(Chart $chart)
    {
        $disburse = $this->disburse->newQuery()->latest()->paginate(10);
        $chart_disburse = $chart->queryChart($this->disburse->newQuery(), 12);
        return view('_disburse.home', compact('disburse', 'chart_disburse'));
    }

    /**
     *  @return \Illuminate\Contracts\Support\Renderable
     */
    public function unconfirmed()
    {
        $disburse = $this->disburse->newQuery()
            ->where('rejected', false)
            ->where('approved', false)
            ->latest()
            ->paginate(10);
        return view('_disburse.unconfirmed', compact('disburse'));
    }

    /**
     *  @return \Illuminate\Contracts\Support\Renderable
     */
    public function rejected()
    {
        $disburse = $this->disburse->newQuery()
            ->where('rejected', true)
            ->where('approved', false)
            ->latest()
            ->paginate(10);
        return view('_disburse.rejected', compact('disburse'));
    }

    /**
     *  @return \Illuminate\Contracts\Support\Renderable
     */
    public function search(Request $request, ParamsQuery $pm)
    {
        $disburse =  $pm->queryByDateParams($request, $this->disburse->newQuery());;
        return view('_disburse.search', compact('disburse'));
    }

    /**
     *  @return \Illuminate\Contracts\Support\Renderable
     */
    public function sortie()
    {
        return view('_disburse.sortie');
    }

    /**
     *  @return \Illuminate\Contracts\Support\Renderable
     *  @return  array
     */
    public function newSortie(Request $request)
    {
        $request->validate([
            'montant' => ['required', 'numeric', 'min:0.1'],
            'motif' => ['required', 'string', 'min:3'],
            'recipient' => ['required', 'string'],
        ]);
        $amount = doubleval($request->montant);
        $this->disburse->newQuery()->create([
            'amount' => $amount,
            'motif' => $request->motif,
            'recipient' => $request->recipient,
            'approved' => ($amount <= 5)
        ]);

        return ['message' => trans($amount <= 5 ? 'Sortie Enregistré et Confirmé' : 'Sortie Enregistré et en attente de confirmation')];
    }
}
