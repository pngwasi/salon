<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function changePassword(Request $request) {
        $request->validate([
            'motDePasse' => ['required', 'string', 'max:255', 'password:web'],
            'nouveau_motDePasse' => ['required', 'string', 'max:255', 'min:6', 'confirmed'],
        ]);
        $user = $request->user('web');
        $filled = $user->fill([
            'password' => Hash::make($request->get('nouveau_motDePasse'))
        ]);
        $filled->save();
        return redirect()->intended($request->redirect)->with('saved', 'Enregistré !');
    }

    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function report()
    {
        return view('report');
    }
}
