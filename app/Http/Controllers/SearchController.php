<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $search
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function search(\Illuminate\Database\Eloquent\Builder $query, ?string $search)
    {
        return $query->where('name', 'LIKE', "%{$search}%")
            ->orWhere('email', 'LIKE', "%{$search}%")
            ->orWhere('phone', 'LIKE', "%{$search}%")
            ->limit(5)
            ->get()
            ->filter(fn ($a) => $a->active);
    }

    /**
     * @param Request $request
     * @param Subscriber $subscriber
     * @return array
     */
    public function subscriber(Request $request, Subscriber $subscriber)
    {
        $query = trim($request->query('search'));
        $subscribers = $this->search($subscriber->newQuery(), $query);
        return $subscribers;
    }

    /**
     * @param Request $request
     * @param User $user
     * @return array
     */
    public function worker(Request $request, User $user)
    {
        $query = trim($request->query('search'));
        $workers = $this->search($user->newQuery(), $query);
        return $workers;
    }
}
