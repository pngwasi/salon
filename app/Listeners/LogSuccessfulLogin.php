<?php

namespace App\Listeners;

use App\Models\LoginActivity;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LogSuccessfulLogin
{

    private LoginActivity $loginActiviy;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(LoginActivity $loginActivity)
    {
        $this->loginActiviy = $loginActivity;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->user->loginActivities()->create([]);
    }
}
