<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = ['start', 'end', 'amount', 'per'];
    
    /**
     * @return boolean
     */
    public function isValid(): bool {
        return date('Y-m-d', strtotime($this->end)) > date('Y-m-d') && $this->active;
    }
}
