<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class salaryAdvance extends Model
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $table = 'salary_advances';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = ['amount', 'salary', 'approved', 'rejected'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
