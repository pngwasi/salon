<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoginActivity extends Model
{
    protected $table = 'login_activities';

    /**
     * Get the owning logable model.
     */
    public function logable()
    {
        return $this->morphTo();
    }
}
