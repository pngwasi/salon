<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialStock extends Model
{
    protected $table = 'materials_stock';

    protected $fillable = ['status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function material() {
        return $this->belongsTo('App\Models\Material');
    }
}
