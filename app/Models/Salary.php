<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $table = 'salaries';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = ['amount', 'expected', 'approved', 'rejected'];

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
