<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PayType extends Model
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $table = 'pay_types';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = ['name'];
}
