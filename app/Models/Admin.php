<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    protected $table = 'admins';

    /**
     * Undocumented variable
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'active', 'super'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function loginActivities()
    {
        return $this->morphMany('App\Models\LoginActivity', 'logable');
    }
}
