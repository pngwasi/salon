<?php

namespace App\Models\Format;

use Illuminate\Support\Facades\DB;

class Chart
{
    public function __construct()
    {
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $model
     * @return array
     */
    public function queryChart($model, int $limit = 6): array
    {
        $datas = [
            'labels' => [],
            'data' => []
        ];
        $result = $model->select(DB::raw('YEAR(created_at) as year, MONTHNAME(created_at) as month, SUM(amount) as sum_amount'))
            ->where('approved', true)
            ->groupByRaw('MONTHNAME(created_at)')
            ->groupByRaw('YEAR(created_at)')
            ->latest()
            ->limit($limit)
            ->get();
        foreach ($result as $row) {
            array_push($datas['labels'], "{$row->month} {$row->year}");
            array_push($datas['data'], $row->sum_amount);
        }
        return $datas;
    }
}
