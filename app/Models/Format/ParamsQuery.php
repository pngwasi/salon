<?php

namespace App\Models\Format;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ParamsQuery
{
    public function __construct()
    {
    }

    /**
     * @param mixed $model
     * @param string|null $day
     * @param string|null $month
     * @param string|null $year
     * @return mixed
     */
    private function performQuery($model, ?string $day, ?string $month, ?string $year)
    {
        $query = null;
        if ($year && is_numeric($year) && Str::length($year) == 4) {
            $query = $model->whereYear('created_at', $year);
            if ($month && is_numeric($month)) {
                $query = $query->whereMonth('created_at', $month);
                if ($day && is_numeric($day)) {
                    $query = $query->whereDay('created_at', $day);
                }
            }
        }
        return $query ?: $model;
    }

    /**
     * @param Request $request
     * @param mixed $model
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|Builder
     */
    public function queryByDateParams(Request $request, $model, $limit = 12, $paginate = true)
    {
        $query = $this->performQuery(
            $model,
            $request->query('day'),
            $request->query('month'),
            $request->query('year')
        );
        if ($paginate) {
            $query = $query->latest()->paginate($limit);
        }
        return $query;
    }

    /**
     * @param mixed $query
     * @param string|null $search
     * @param int $limit
     * 
     */

    /**
     * @param Request $request
     * @param mixed $model
     * @param string $keyquery
     * @param integer $limit
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function search(Request $request, $model, string $keyquery = 'search', $limit = 5)
    {
        $query = null;
        if ($search = $request->query($keyquery)) {
            $query = $model->where('name', 'LIKE', "%{$search}%")
                ->orWhere('email', 'LIKE', "%{$search}%")
                ->orWhere('phone', 'LIKE', "%{$search}%")->latest()->limit($limit);
        }
        return $query ?: $model;
    }
}
