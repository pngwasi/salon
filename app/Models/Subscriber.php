<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $tables = 'subscribers';

    protected $fillable = ['name', 'email', 'phone', 'address'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany('App\Models\Subscription');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hairdress()
    {
        return $this->hasMany('App\Models\Hairdressing');
    }
}
