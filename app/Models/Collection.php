<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $table = 'collections';

    protected $fillable = ['amount', 'origin', 'description', 'approved', 'rejected', 'pay_type_id'];
}
