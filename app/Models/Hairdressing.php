<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hairdressing extends Model
{
    protected $table = 'hairdressing';

    protected $fillable = [
        'amount', 'approved', 'rejected', 'pay_type_id', 'subscriber_id', 'specification_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subscriber()
    {
        return $this->belongsTo('App\Models\Subscriber');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function modePay()
    {
        return $this->belongsTo('App\Models\PayType', 'pay_type_id');
    }

        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specification()
    {
        return $this->belongsTo('App\Models\Specification');
    }
}
