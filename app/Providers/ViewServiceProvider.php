<?php

namespace App\Providers;

use App\View\Components\ReportModal;
use App\View\Components\UserModify;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('user-modify', UserModify::class);
        Blade::component('report-modal', ReportModal::class);
    }
}
