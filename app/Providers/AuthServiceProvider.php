<?php

namespace App\Providers;

use App\Policies\CashierPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-cashier', function ($user) {
            return !!$user->cashier;
        });
        Gate::define('not-cashier', function ($user) {
            return !$user->cashier;
        });
        Gate::define('view-super', function ($user) {
            return !!$user->super;
        });
    }
}
