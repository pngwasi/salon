<?php

namespace App\Report;

use App\Models\Disbursement;
use App\Models\Hairdressing;
use App\Models\Material;
use App\Models\Specification;
use App\Models\Subscriber;
use App\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ReportTable
{
    /**
     * Undocumented variable
     *
     * @var PDF
     */
    private $pdf;

    /**
     * Undocumented variable
     *
     * @var string
     */
    private $tp = 'template.reports';

    /**
     * Undocumented function
     *
     * @param PDF $pdf
     */
    public function __construct(PDF $pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * Undocumented function
     *
     * @param static $model
     * @param string $view
     * @return void
     */
    private function modelView($model, $view)
    {
        $html = view("{$this->tp}.{$view}", compact('model'))->toHtml();
        return $this->pdf->loadHTML($html)->stream(Str::slug("{$model->id} rapport", '_') . '.pdf');
    }

    /**
     * Undocumented function
     * @param static $model
     * @return \Illuminate\Http\Response
     */
    public function invoiceHairdress($model)
    {
        return $this->modelView($model, 'Invoices.invoice-hairdress');
    }

    /**
     * Undocumented function
     *
     * @param array $calls
     * @return \Illuminate\Http\Response
     */
    public function generate(array $calls)
    {
        $html = '';
        foreach ([
            'list-workers' => '@usersList',
            'list-material' => '@materialList',
            'list-subscriber' => '@subscriberList',
            'list-specification' => '@specificationList'
        ] as $key => $function) {
            if (in_array($key, $calls)) {
                $html .= app()->call(self::class . $function);
            }
        }
        if (empty($html)) {
            $html .= app()->call(self::class . '@generalReport');
        }
        return $this->pdf->loadHTML($html)->stream();
    }
    /**
     * Undocumented function
     * @param static $model
     * @return \Illuminate\Http\Response
     */
    public function disburse($model)
    {
        return $this->modelView($model, 'Invoices.disburse');;
    }

    /**
     * Undocumented function
     * @param static $model
     * @return \Illuminate\Http\Response
     */
    public function salary($model)
    {
        return $this->modelView($model, 'Invoices.salary');;
    }

    /**
     * Undocumented function
     * @param static $model
     * @return \Illuminate\Http\Response
     */
    public function salaryModerator($model)
    {
        return $this->modelView($model, 'Invoices.salary-moderator');;
    }

    /**
     * Undocumented function
     *
     * @param User $user
     * @return string
     */
    public function usersList(User $user)
    {
        return view("{$this->tp}.lists.user-list", ['users' => $user::all()]);
    }

    /**
     * Undocumented function
     *
     * @param Subscriber $subscriber
     * @return string
     */
    public function subscriberList(Subscriber $subscriber)
    {
        return view("{$this->tp}.lists.subscriber-list", ['subscribers' => $subscriber::all()]);
    }

    /**
     * Undocumented function
     *
     * @param Material $material
     * @return string
     */
    public function materialList(Material $material)
    {
        return view("{$this->tp}.lists.material-list", ['materials' => $material::all()]);
    }

    /**
     * Undocumented function
     *
     * @param Specification $specification
     * @return string
     */
    public function specificationList(Specification $specification)
    {
        return view("{$this->tp}.lists.specification-list", ['specifications' => $specification::all()]);
    }

    /**
     * Undocumented function
     *
     * @param Hairdressing $model
     * @return array
     */
    private function getWorkingYears(Hairdressing $model)
    {
        return $model->newQuery()->select(DB::raw('YEAR(created_at) as year'))
            ->groupByRaw('YEAR(created_at)')
            ->latest()
            ->get()
            ->map(fn ($y) => $y->year)
            ->all();
    }

    /**
     * Undocumented function
     *
     * @param Model $model
     * @param string $year
     * @return Builder
     */
    private function query($model, $year)
    {
        return $model->newQuery()->whereYear('created_at', $year);
    }
    /**
     * Undocumented function
     *
     * @param Hairdressing $model
     * @param string $year
     * @return array
     */
    private function entriesByYear(Hairdressing $model, $year): array
    {
        return [
            'approved' => $this->query($model, $year)->where('approved', true)->sum('amount'),
            'rejected' => $this->query($model, $year)->where('rejected', true)->sum('amount'),
        ];
    }

    /**
     * Undocumented function
     *
     * @param Disbursement $model
     * @param string $year
     * @return array
     */
    private function disburseByYear(Disbursement $model, $year): array
    {
        return [
            'approved' => $this->query($model, $year)->where('approved', true)->sum('amount'),
            'rejected' => $this->query($model, $year)->where('rejected', true)->sum('amount'),
        ];
    }

    /**
     * Undocumented function
     *
     * @param User $model
     * @param string $year
     * @return int
     */
    private function usersByYear(User $model, $year)
    {
        return $this->query($model, $year)->count();
    }

    /**
     * Undocumented function
     *
     * @param Subscriber $model
     * @param string $year
     * @return int
     */
    private function subscriberByYear(Subscriber $model, $year)
    {
        return $this->query($model, $year)->count();
    }

    /**
     * Undocumented function
     *
     * @param User $model
     * @param string $year
     * @return int
     */
    private function materialByYear(Material $model, $year)
    {
        return $this->query($model, $year)->count();
    }

    /**
     * Undocumented function
     *
     * @return string
     */
    public function generalReport(Hairdressing $hairdressing, Disbursement $disbursement, User $user, Subscriber $subscriber, Material $material)
    {
        $reports = [];
        $years = $this->getWorkingYears($hairdressing);
        foreach ($years as $year) {
            $reports[] = [
                'year' => $year,
                'entry' => $this->entriesByYear($hairdressing, $year),
                'disburse' => $this->disburseByYear($disbursement, $year),
                'users' => $this->usersByYear($user, $year),
                'subscribers' => $this->subscriberByYear($subscriber, $year),
                'materials' => $this->materialByYear($material, $year)
            ];
        }
        $toYears = count($years) ? $years[0] . " - " . last($years) : '-';
        return view("{$this->tp}.general", compact('reports', 'toYears'))->toHtml();
    }
}
