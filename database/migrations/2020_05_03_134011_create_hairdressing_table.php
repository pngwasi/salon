<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHairdressingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hairdressing', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable()->references('id')->on('users')->onDelete('set null');
            $table->foreignId('pay_type_id')->nullable()->references('id')->on('pay_types')->onDelete('set null');
            $table->foreignId('subscriber_id')->nullable()->references('id')->on('subscribers')->onDelete('set null');
            $table->foreignId('specification_id')->nullable()->references('id')->on('specifications')->onDelete('set null');
            $table->decimal('amount')->default(0.00);
            $table->boolean('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hairdressing');
    }
}
