const loadingOverlay = document.querySelector('.loading-ntf');


export default class FL {

    static get contains() {
        return loadingOverlay.classList.contains('hidden')
    }

    static set() {
        if (this.contains) {
            loadingOverlay.classList.remove('hidden');
        }
    }

    static unset() {
        if (!this.contains) {
            return loadingOverlay.classList.add('hidden');
        }
    }

    /**
     * @param {HTMLElement} parent 
     */
    static prSet(parent) {
        parent.style.position = 'relative';
        const exist = parent.querySelector('.loading-ntf-abs')
        if (exist) return;
        const div = document.createElement('div');
        div.className = 'loading-ntf-abs';
        div.innerHTML = `
        <div class='uil-ring-css-abs'>
            <spinning-dots style="
                    width:40px;
                    color: #27a3eb;" />
        </div>`;
        parent.appendChild(div)
    }

    /**
     * @param {HTMLElement} parent 
    */
    static prUnset(parent) {
        const exist = parent.querySelector('.loading-ntf-abs');
        if (exist) exist.parentNode.removeChild(exist);
    }

}