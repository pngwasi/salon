
/**
 * @param { HTMLElement } element 
 * @param { } datas 
 * @param { string } url 
 * @param { ChartTooltipCallback } formCallback 
 */
const scriptToForm = (element, datas = {}, url, formCallback) => {
    const keys = Object.keys(datas)
    const modal = element.querySelector('#modif-users-modal')
    const form = modal.querySelector('#modif-users-form')
    keys.forEach(key => {
        const input = form.querySelector(`[name="${key}"]`)
        const value = datas[key]
        if (input) {
            input.type === 'checkbox' ? input.checked = !!value : input.value = value
        }
    })
    form.action = url
    form.addEventListener('submit', formCallback)
    $(modal).modal('show')
}

/**
 * @param { HTMLElement } element 
 * @param { CallableFunction } formCallback 
 */
export const UserModif = (element, formCallback) => {
    Array.from(element.querySelectorAll('.modif-users'))
        .forEach((el) => el.addEventListener('click', (e) => {
            e.stopPropagation()
            e.preventDefault()
            const script = el.parentNode.querySelector(`#modif-users-${el.dataset.id}`)
            scriptToForm(element, JSON.parse(script.textContent), el.href, formCallback)
        }))
}