// import Swup from 'swup';
// import SwupScrollPlugin from '@swup/scroll-plugin';
import NProgress from 'nprogress';
import { Axios } from '../api/api';
import FL from './Loading';

const Instance = {
    swup: null
}

export default class Utils {

    static get swup() {
        return Instance.swup
    }

    static notifyExceptionMessage({ data: { message, errors } }) {
        let text = message;
        if (typeof errors === 'object') {
            text += '<br>'
            for (const key in errors) {
                text += `${errors[key].join(' ')}<br>`
            }
        }
        window.$.notify({
            icon: 'add_alert',
            message: text
        }, {
            type: "danger",
            z_index: 11055,
            placement: {
                from: "bottom",
                align: "right"
            },
        })
    }

    static interceptor() {
        Axios.interceptors.request.use(req => {
            FL.set()
            return req
        })
        Axios.interceptors.response.use(res => {
            FL.unset()
            if (res.data.message) {
                window.$.notify({
                    message: res.data.message
                }, {
                    type: "success",
                    z_index: 11055,
                    placement: {
                        from: "bottom",
                        align: "right"
                    },
                })
            }
            return res
        }, (error) => {
            FL.unset()
            this.notifyExceptionMessage(error.response)
            return Promise.reject(error)
        })
    }

    static dropify() {
        $('.dropify').dropify();
    }

    static init() {
        this.interceptor()
        const h = document.querySelector('#nav-title')
        if (!h) return
        const swup = new Swup({
            containers: ["#app-dashboard", "#nav-title", '#sidebar-wrapper'],
            plugins: [new SwupScrollPlugin()],
            cache: false
        });
        Instance.swup = swup
        this.Nprogress(swup)
        return swup
    };

    static get INprogress() {
        NProgress.configure({
            minimum: 0.4
        })
        const nprogress = {
            unset() {
                NProgress.done()
            },
            set() {
                NProgress.start()
            }
        }
        return nprogress
    }
    /**
     * @param {Swup} swup 
     */
    static Nprogress(swup) {
        const nprogress = this.INprogress
        swup.on('transitionStart', () => nprogress.set())
        swup.on('contentReplaced', () => nprogress.unset())
    }
}


export const number_format = (number, decimals, dec_point, thousands_sep) => {
    // *     example: number_format(1234.56, 2, ',', ' ');
    // *     return: '1 234,56'
    number = (number + '').replace(',', '').replace(' ', '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}