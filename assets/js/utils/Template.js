/**
 * @returns { DocumentFragment }
 * @param {Array} datas 
 */
export const listItemsFragment = (datas = []) => {
    const text = !datas.length ? `<div class="text-center">Aucun resultat</div>` : datas.map(element => `
            <div class="list-group" data-name-target="${element.name}" data-phone-target="${element.phone}" data-id-target="${element.id}">
                <a style="cursor:pointer;" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                        <h5>${element.name}</h5>
                    </div>
                    <div>Email: ${element.email}</div>
                    <div><small>Tel: ${element.phone}</small></div>
                </a>
            </div>
        `).join('\n')
    return document.createRange().createContextualFragment(text)
}

/**
 * @param { HTMLElement } item 
 */
export const removeItemsChild = (item) => {
    Array.from(item.children)
        .forEach(element => element.parentNode.removeChild(element))
}