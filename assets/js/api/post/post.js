import Api from "../api"

/**
 * @param  {Event} e
 * @returns { boolean|Object }
 */
export const PostCall = async (e, norefresh = null) => {
    e.preventDefault()
    const form = new FormData(e.target)
    try {
        const { data } = await Api.post(e.target.action, form)
        if (!norefresh) {
            window.setTimeout(() => window.location.reload(), 1500)
        }
        return data
    } catch (_) {
        return false
    }
}