import Utils from "../../utils/utils"
import { ApiAdmin } from "../api"

/**
 * 
 * @param { string } id 
 * @param { HTMLElement } target 
 * @returns { object }
 */
export const getProfileAsync = async (id, url) => {
    Utils.INprogress.set()
    let datas = []
    try {
        const { data } = await ApiAdmin.get(`${url}?id=${id}`)
        datas = data
    } catch (error) {
        datas = {}
    }
    Utils.INprogress.unset()
    return datas
}