import { ApiAdmin } from "../api"
import Utils from "../../utils/utils"

/**
 * @param {EventTarget} target 
 * @returns { Array }
 */
export const searchAction = async (target) => {
    Utils.INprogress.set()
    let datas = []
    try {
        const { data } = await ApiAdmin.get(`${target.getAttribute('data-url')}?search=${target.value}`)
        datas = data
    } catch (error) {
        datas = []
    }
    Utils.INprogress.unset()
    return datas
}