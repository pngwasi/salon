import { Controller } from "stimulus"
import { PostCall } from "../api/post/post"
import { UserModif } from "../utils/UserModif"

export default class extends Controller {

    initialize() { }

    connect() {
        UserModif(document, PostCall)
    }

    /**
     * @param  {Event} e 
     */
    newMaterial = (e) => PostCall(e)

}
