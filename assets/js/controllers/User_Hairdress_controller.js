import { Controller } from "stimulus"
import FL from "../utils/Loading"
import { ChartLine, ChartBar } from "../utils/Chart"

export default class extends Controller {

    initialize() {
        const hasChart = this.element.querySelector('#chart-home')
        hasChart && this.chart(hasChart)
    }
    /**
     * @param { HTMLElement } element 
     */
    hairdressChart(element) {
        ChartLine(
            this.chartjs,
            element.querySelector('#heardress'),
            window.chart_hairdress.labels,
            window.chart_hairdress.data
        )
        // backgroundColor: ['#29B0D0']
    }
    /**
     * @param { HTMLElement } element 
     */
    bonusChart(element) {
        ChartBar(
            this.chartjs,
            element.querySelector('#bonus'),
            window.chart_bonus.labels,
            window.chart_bonus.data
        )
    }

    /**
     * @param { HTMLElement } element 
     */
    async chart(element) {
        FL.prSet(element)
        this.chartjs = (await import('chart.js')).default
        FL.prUnset(element)
        this.hairdressChart(element)
        this.bonusChart(element)
    }

    connect() { }

}
