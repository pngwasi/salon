import { Controller } from "stimulus"
import Api from "../api/api"

export default class extends Controller {

    initialize() { }

    connect() {
        this.performInputStatus()
    }

    /**
     * 
     * @param { HTMLInputElement } target 
     * @param { boolean } status 
     */
    changeStatusInput(target, status) {
        if (!target) return
        target = target.parentNode.parentNode
        const input = Array.from(target.querySelectorAll('input[type=checkbox]'))
        input.length && input.forEach(el => (el.disabled = status))
    }

    performInputStatus() {
        let target = null
        Array.from(this.element.querySelectorAll('[name="report-select"]'))
            .forEach((element) => {
                element.addEventListener('change', (e) => {
                    this.changeStatusInput(e.target, false)
                    this.changeStatusInput(target, true)
                    target = e.target
                })
            })
    }

    /**
     * @param { Event } e
     */
    report = (e) => {
        e.preventDefault()
        const { target: form } = e
        const datas = new URLSearchParams(new FormData(form))
        const url = `${form.action}?${datas.toString()}`
        window.dispatchEvent(new CustomEvent('report-modal', {
            detail: { url }
        }))
    }

}
