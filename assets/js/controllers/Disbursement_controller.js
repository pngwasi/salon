import { Controller } from "stimulus"
import { PostCall } from "../api/post/post"

export default class extends Controller {

    initialize() { }

    connect() { }

    /**
     * @param  {Event} e 
     */
    newMaterial = (e) => PostCall(e)

}
