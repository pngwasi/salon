import { Controller } from "stimulus"
import { ChartLine, ChartBar } from "../utils/Chart"

export default class extends Controller {

    initialize() {
        this.chart()
    }

    connect() { }

    /**
     * @param { HTMLElement } element 
     */
    async chart() {
        this.chartjs = (await import('chart.js')).default
        ChartLine(
            this.chartjs,
            document.getElementById("collect-chart"),
            window.chart_collect.labels,
            window.chart_collect.data,
        )
        ChartBar(
            this.chartjs,
            document.getElementById("disburse-chart"),
            window.chart_disburse.labels,
            window.chart_disburse.data,
        )
    }
}
