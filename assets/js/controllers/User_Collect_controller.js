import { Controller } from "stimulus"
import FL from "../utils/Loading"
import { ChartLine } from "../utils/Chart"
import { debounce } from 'lodash'
import { removeItemsChild, listItemsFragment } from "../utils/Template"
import { searchAction } from "../api/search/search"
import { getProfileAsync } from "../api/profile/profile"
import { PostCall } from "../api/post/post"


export default class extends Controller {

    static targets = ['hls', 'hlsList', 'hlsCard', 'scb', 'scbList', 'scbCard', 'hlsS', 'hlsSList']

    initialize() {
        const hasChart = this.element.querySelector('#chart-home')
        hasChart && this.chart(hasChart)
    }

    connect() {
        try {
            this.scb && this.scb.addEventListener('keypress', debounce(this.subscribersSeachAction, 500))
        } catch (_) { }
        try {
            this.hls && this.hls.addEventListener('keypress', debounce(this.workersSeachAction, 500))
        } catch (_) { }
        try {
            this.hlsS && this.hlsS.addEventListener('keypress', debounce(this.workersSeachActionOnSearchField, 500))
        } catch (_) { }
    }
    /**
     * @param { HTMLElement } element 
     */
    collectChart(element) {
        ChartLine(
            this.chartjs,
            element.querySelector('#collect_chart'),
            window.chart_collect.labels,
            window.chart_collect.data
        )
        // backgroundColor: ['#2A516E']
    }

    /**
     * @param { HTMLElement } element 
     */
    async chart(element) {
        FL.prSet(element)
        this.chartjs = (await import('chart.js')).default
        FL.prUnset(element)
        this.collectChart(element)
    }

    reset = () => {
        const y = this.element.querySelector('#specification');
        y && (y.value = '')
        const j = this.element.querySelector('#mode_pay')
        j && (j.value = '')
        try {
            this.workerProfilHtml({})
        } catch (_) { }
        try {
            this.subscriberProfilHtml({})
        } catch (_) { }

    }

    specificationValue = e => {
        const y = event.target;
        const u = y.querySelector(`[value="${y.value}"]`).getAttribute('data-amount');
        this.element.querySelector('#montant').value = u
    }

    newEntry = async (e) => {
        const res = await PostCall(e, true)
        if (res) {
            this.reset()
            window.dispatchEvent(new CustomEvent('report-modal', {
                detail: {
                    url: res.reportUrl
                }
            }))
        }

    }

    newBonus = async (e) => (await PostCall(e, true)) && this.reset()

    /**
     * @param {string} name 
     * @param {string} value 
     */
    attrWorker(name, value) {
        const y = this.hlsCard.querySelector(`[data-${name}]`);
        y && (y.textContent = (value !== null ? value : ''))

    }

    /**
     * @param {string} name 
     * @param {string} value 
     */
    attrSubscriber(name, value) {
        const y = this.scbCard.querySelector(`[data-${name}]`);
        y && (y.textContent = (value !== null ? value : ''))

    }

    /**
     * 
     * @param { HTMLInputElement } element 
     * @param { string } value 
     */
    insetValue(element, value) {
        element && (element.value = value || '')
    }

    /**
     * @param { Object } data 
     */
    workerProfilHtml(data) {
        this.insetValue(this.element.querySelector('[name=coiffeur]'), data.id)
        this.insetValue(this.element.querySelector('[name=coiffeur_email]'), data.email)
        this.insetValue(this.element.querySelector('#worker_name'), data.name)
        this.attrWorker('name', data.name)
        this.attrWorker('email', data.email)
        this.attrWorker('phone', data.phone)
        this.attrWorker('amount-h', data.sum_hairdress)
        this.attrWorker('total-h', data.count_hairdress)
        this.attrWorker('bonus', data.sum_bonus)
    }

    /**
     * @param { Object } data 
     */
    subscriberProfilHtml(data) {
        this.insetValue(this.element.querySelector('[name=abonne]'), data.id)
        this.insetValue(this.element.querySelector('[name=abonne_email]'), data.email)
        this.insetValue(this.element.querySelector('#subscriber_name'), data.name)
        this.insetValue(this.element.querySelector('#montant'), data.per_hairdress)
        this.attrSubscriber('name', data.name)
        this.attrSubscriber('email', data.email)
        this.attrSubscriber('phone', data.phone)
        this.attrSubscriber('amount-scp', data.subscription_status)
        this.attrSubscriber('total-hm', data.total_hairdress)
        this.attrSubscriber('per-h', data.per_hairdress)
        this.attrSubscriber('rest-h', data.rest_hairdress)
    }

    /**
     * @param { DocumentFragment } html
     */
    performWorkerProfile(html) {
        Array.from(html.querySelectorAll('[data-phone-target]'))
            .forEach(child => child.addEventListener('click', async () => {
                FL.prSet(this.hlsCard)
                this.workerProfilHtml((await getProfileAsync(
                    child.getAttribute('data-id-target'),
                    this.hlsCard.getAttribute('data-url')
                )))
                FL.prUnset(this.hlsCard)
            }))
    }

    /**
     * @param { DocumentFragment } html
     */
    performSubscriberProfile(html) {
        Array.from(html.querySelectorAll('[data-phone-target]'))
            .forEach(child => child.addEventListener('click', async () => {
                FL.prSet(this.scbCard)
                this.subscriberProfilHtml((await getProfileAsync(
                    child.getAttribute('data-id-target'),
                    this.scbCard.getAttribute('data-url')
                )))
                FL.prUnset(this.scbCard)
            }))
    }

    /**
     * @param { Event } e
     */
    workersSeachAction = async (e) => {
        const datas = await searchAction(e.target)
        removeItemsChild(this.hlsList)
        const html = listItemsFragment(datas)
        this.performWorkerProfile(html)
        this.hlsList.appendChild(html)
    }

    /**
     * @param { Event } e
     */
    subscribersSeachAction = async (e) => {
        const datas = await searchAction(e.target)
        removeItemsChild(this.scbList)
        const html = listItemsFragment(datas)
        this.performSubscriberProfile(html)
        this.scbList.appendChild(html)
    }

    workersSeachActionOnSearchField = async (e) => {
        const datas = await searchAction(e.target)
        removeItemsChild(this.hlsSList)
        const html = listItemsFragment(datas)
        Array.from(html.querySelectorAll('[data-phone-target]'))
            .forEach(child => child.addEventListener('click', () => {
                this.element.querySelector('#worker_nameS').value = child.getAttribute('data-name-target')
                this.element.querySelector('[name="uid"]').value = child.getAttribute('data-id-target')
            }))
        this.hlsSList.appendChild(html)
    }

    /**
     * @returns {HTMLInputElement}
    */
    get hlsCard() {
        return this.hlsCardTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get scbCard() {
        return this.scbCardTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get hls() {
        return this.hlsTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get hlsList() {
        return this.hlsListTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get hlsS() {
        return this.hlsSTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get hlsSList() {
        return this.hlsSListTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get scb() {
        return this.scbTarget
    }
    /**
     * @returns {HTMLInputElement}
    */
    get scbList() {
        return this.scbListTarget
    }
}
