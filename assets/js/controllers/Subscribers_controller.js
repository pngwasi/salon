import { Controller } from "stimulus"
import { PostCall } from "../api/post/post"
import { debounce } from 'lodash'
import datepicker from 'js-datepicker'
import { searchAction } from "../api/search/search"
import { listItemsFragment, removeItemsChild } from "../utils/Template"
import { getProfileAsync } from "../api/profile/profile"
import FL from "../utils/Loading"
import { UserModif } from "../utils/UserModif"


export default class extends Controller {

    static targets = ['subscribersSeach', 'subscribersList', 'subscriberField', 'scb', 'scbList', 'scbCard']

    initialize() {
        datepicker('#date_entry', {
            showAllDates: true,
            formatter: (input, date, instance) => {
                const d = this.formatDate(date)
                input.value = `${d.ye}-${d.mo}-${d.da}`
            }
        })
        datepicker('#date_expire', {
            showAllDates: true,
            formatter: (input, date, instance) => {
                const d = this.formatDate(date)
                input.value = `${d.ye}-${d.mo}-${d.da}`
            }
        })
    }

    formatDate(date) {
        const dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: '2-digit', day: '2-digit' })
        const [{ value: mo }, , { value: da }, , { value: ye }] = dtf.formatToParts(date)
        return { mo, da, ye }
    }

    connect() {
        UserModif(document, PostCall)
        try {
            this.subscribersSeach && this.subscribersSeach.addEventListener('keypress', debounce(this.subscribersSeachAction, 500))
        } catch (_) { }
        try {
            this.scb && this.scb.addEventListener('keypress', debounce(this.subscribersSeachActionProfile, 500))
        } catch (_) { }
    }

    /**
     * @param {string} name 
     * @param {string} value 
     */
    attrSubscriber(name, value) {
        const y = this.scbCard.querySelector(`[data-${name}]`);
        y && (y.textContent = (value !== null ? value : ''))
    }
    /**
     * @param { HTMLInputElement } element 
     * @param { string } value 
     */
    insetValue(element, value) {
        element && (element.value = value || '')
    }


    /**
     * @param { DocumentFragment } html
     */
    bindEventToElement(html) {
        Array.from(html.querySelectorAll('[data-phone-target]'))
            .forEach(child => child.addEventListener('click', () => (this.subscriberField.value = child.getAttribute('data-phone-target'))))
    }

    /**
     * @param { Object } data 
     */
    subscriberProfilHtml(data) {
        this.insetValue(this.element.querySelector('#subscriber_name'), data.name)
        this.attrSubscriber('name', data.name)
        this.attrSubscriber('email', data.email)
        this.attrSubscriber('phone', data.phone)
        this.attrSubscriber('amount-scp', data.subscription_status)
        this.attrSubscriber('total-hm', data.total_hairdress)
        this.attrSubscriber('per-h', data.per_hairdress)
        this.attrSubscriber('rest-h', data.rest_hairdress)
    }
    /**
     * @param { DocumentFragment } html
     */
    performSubscriberProfile(html) {
        Array.from(html.querySelectorAll('[data-phone-target]'))
            .forEach(child => child.addEventListener('click', async () => {
                FL.prSet(this.scbCard)
                this.subscriberProfilHtml((await getProfileAsync(
                    child.getAttribute('data-id-target'),
                    this.scbCard.getAttribute('data-url')
                )))
                FL.prUnset(this.scbCard)
            }))
    }


    /**
     * @param { Event } e
     */
    subscribersSeachActionProfile = async (e) => {
        const datas = await searchAction(e.target)
        removeItemsChild(this.scbList)
        const html = listItemsFragment(datas)
        this.performSubscriberProfile(html)
        this.scbList.appendChild(html)
    }


    /**
     * @param { Event } e
     */
    subscribersSeachAction = async (e) => {
        const datas = await searchAction(e.target)
        removeItemsChild(this.subscribersList)
        const html = listItemsFragment(datas)
        if (e.target.hasAttribute('data-replace')) {
            Array.from(html.querySelectorAll('[data-phone-target]'))
                .forEach(el => {
                    el.onclick = () => {
                        const id = el.getAttribute('data-id-target')
                        window.location.replace(`${window.location.pathname}?id=${id}`)
                    }
                })
        } else {
            this.bindEventToElement(html)
        }
        this.subscribersList.appendChild(html)
    }

    /**
     * @param  {Event} e 
     */
    newSubscriber = (e) => PostCall(e)
    /**
     * @param  {Event} e 
     */
    newSubscription = (e) => PostCall(e)

    /**
     * @returns {HTMLInputElement}
    */
    get subscriberField() {
        return this.subscriberFieldTarget
    }
    /**
     * @returns {HTMLElement}
    */
    get subscribersSeach() {
        return this.subscribersSeachTarget
    }

    /**
     * @returns {HTMLElement}
    */
    get subscribersList() {
        return this.subscribersListTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get scb() {
        return this.scbTarget
    }
    /**
     * @returns {HTMLInputElement}
    */
    get scbList() {
        return this.scbListTarget
    }
    /**
     * @returns {HTMLInputElement}
    */
    get scbCard() {
        return this.scbCardTarget
    }
}
