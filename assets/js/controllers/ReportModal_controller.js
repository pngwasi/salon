import { Controller } from "stimulus"
import FL from "../utils/Loading";

export default class extends Controller {

    initialize() {
        const element = this.el();
        this.object = element.cloneNode()
        this.parent = element.parentNode;
        this.parent.removeChild(element)
        this.modalBody = this.element.querySelector('.modal-body')
    }

    /**
     * @returns { HTMLObjectElement }
     */
    el = () => this.element.querySelector('#report-modal')

    connect() {
        Array.from(document.querySelectorAll('.report-modal'))
            .forEach((el) => el.addEventListener('click', () => this.opeanModal(el.dataset.url)))
        this.desconnectModalObject()
        this.triggedEventReport()
    }

    opeanModal(url) {
        this.object.data = url
        this.parent.appendChild(this.object)
        this.loadingDataObject()
        $(this.element).modal('show')
    }

    triggedEventReport() {
        window.addEventListener('report-modal', (e) => {
            this.opeanModal(e.detail.url)
        })
    }

    loadingDataObject() {
        FL.prSet(this.modalBody)
        this.el().addEventListener('load', () => FL.prUnset(this.modalBody))
        this.el().addEventListener('error', () => FL.prUnset(this.modalBody))
    }

    desconnectModalObject() {
        $(this.element).on('hidden.bs.modal', (e) => {
            this.parent.removeChild(this.el());
        })
    }

}
