import { Controller } from "stimulus"
import { PostCall } from "../api/post/post"
import FL from "../utils/Loading"

export default class extends Controller {

    static targets = ['type', 'name', 'body']

    initialize() { }

    connect() {
        Array.from(document.querySelectorAll('[data-target="#contact--sb--wk"]'))
            .forEach((el) => el.addEventListener('click', () => {
                this.name.textContent = el.getAttribute('data-u-name')
                this.type.textContent = `${el.getAttribute('data-u-phone')}, ${el.getAttribute('data-u-email')}`
                FL.prSet(this.body)
                window.setInterval(() => FL.prUnset(this.body), 2000)
            }))
    }

    /**
     * @returns {HTMLInputElement}
    */
    get body() {
        return this.bodyTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get name() {
        return this.nameTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get type() {
        return this.typeTarget
    }

}
