import { Controller } from "stimulus"
import { searchAction } from "../api/search/search"
import { removeItemsChild, listItemsFragment } from "../utils/Template"
import  { debounce } from 'lodash'

export default class extends Controller {

    static targets = ['hlsS', 'hlsSList']

    initialize() {
    }

    connect() {
        try {
            this.hlsS && this.hlsS.addEventListener('keypress', debounce(this.workersSeachActionOnSearchField, 500))
        } catch (_) { }
    }

    workersSeachActionOnSearchField = async (e) => {
        const datas = await searchAction(e.target)
        removeItemsChild(this.hlsSList)
        const html = listItemsFragment(datas)
        Array.from(html.querySelectorAll('[data-phone-target]'))
            .forEach(child => child.addEventListener('click', () => {
                this.element.querySelector('#worker_nameS').value = child.getAttribute('data-name-target')
                this.element.querySelector('[name="uid"]').value = child.getAttribute('data-id-target')
            }))
        this.hlsSList.appendChild(html)
    }

    /**
     * @returns {HTMLInputElement}
    */
    get hlsS() {
        return this.hlsSTarget
    }

    /**
     * @returns {HTMLInputElement}
    */
    get hlsSList() {
        return this.hlsSListTarget
    }


}
