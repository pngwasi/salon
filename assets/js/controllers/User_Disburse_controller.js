import { Controller } from "stimulus"
import FL from "../utils/Loading"
import { ChartLine } from "../utils/Chart"
import { PostCall } from "../api/post/post"

export default class extends Controller {

    initialize() {
        const hasChart = this.element.querySelector('#chart-home')
        hasChart && this.chart(hasChart)
    }

    connect() { }

    /**
     * @param { HTMLElement } element 
     */
    async chart(element) {
        FL.prSet(element)
        this.chartjs = (await import('chart.js')).default
        FL.prUnset(element)
        this.collectChart(element)
    }

    /**
     * @param { HTMLElement } element 
     */
    collectChart(element) {
        ChartLine(
            this.chartjs,
            element.querySelector('#disburse_chart'),
            window.chart_disburse.labels,
            window.chart_disburse.data
        )
        // backgroundColor: ['#F07124']
    }

    /**
     * @param { Event } e
     */
    newSortie = async e => (await PostCall(e, true)) && this.resetField(e.target)

    /**
     * @param {EventTarget } target
     */
    resetField = (target) => {
        const form = new FormData(target)
        form.forEach((v, k) => this.element.querySelector(`[name="${k}"]`).value = '')
    }

}
