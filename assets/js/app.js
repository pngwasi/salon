import '@stimulus/polyfills'
import Popper from 'popper.js'
import $ from 'jquery'
import { Application } from "stimulus"
import { definitionsFromContext } from "stimulus/webpack-helpers"
import Utils from "./utils/utils"
import '@grafikart/spinning-dots-element'

window.Popper = Popper
window.$ = window.jQuery = $
import 'bootstrap'
import 'bootstrap-notify'

Utils.init()
const application = Application.start()
const context = require.context("./controllers", true, /\.js$/)
application.load(definitionsFromContext(context))
