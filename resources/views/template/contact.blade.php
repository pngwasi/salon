<div class="modal fade" data-controller="Contacts" id="contact--sb--wk" data-backdrop="static" data-keyboard="false"
    tabindex="-1" role="dialog" aria-labelledby="contact--sb--wkLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contact--sb--wkLabel">
                    <div class="my-0">
                        <b data-target="Contacts.name">StanIslove</b>
                    </div>
                    <div class="my-0">
                        <small data-target="Contacts.type"></small>
                    </div>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body tchat-contact py-0" data-target="Contacts.body">
                <div class="chat-container">
                    <div class="row h-100 sidebar--p">
                        <div style="width: 12rem;" class="border-chat-lightgray px-0 sidebar--b" id="sidebar">
                            <div id="sidebar-content" class="w-100 h-100">
                                <div class="sidebar-scroll" id="list-group">
                                    <ul class="list-group w-100" id="friend-list">
                                        <li class="list-group-item p-1 active hover-bg-lightgray">
                                            <div class="rounded-circle d-inline-block">
                                                @include('template.svg.sms-svg')
                                            </div>
                                            <span class="d-xs-none username ml-3">Sms</span>
                                        </li>
                                        <li class="list-group-item p-1 hover-bg-lightgray">
                                            @include('template.svg.email-svg')
                                            <span class="d-xs-none username ml-3">E-Mail</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-3"></div>
                        <div class="col-9 d-flex p-0 card-message">
                            <div class="card">
                                <div class="card-body bg-lightgrey d-flex flex-column p-0" style="flex: 9 1">
                                    <div style="min-height: 8rem;" class="container-fluid message-scroll" style="flex: 1 1">
                                        <div class="row">
                                            <div class="card message-card m-1">
                                                <div class="card-body p-2">
                                                    <span class="mx-2">Hi, Dave</span>
                                                    <span class="float-right mx-1"><small>14:13</small></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end">
                                            <div class="card message-card bg-lightblue m-1">
                                                <div class="card-body p-2">
                                                    <span class="mx-2">Hello, Stan</span>
                                                    <span class="float-right mx-1"><small>14:14</small></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end">
                                            <div class="card message-card bg-lightblue m-1">
                                                <div class="card-body p-2">
                                                    <span class="mx-2">What's up?</span>
                                                    <span class="float-right mx-1"><small>14:14</small></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="card message-card m-1">
                                                <div class="card-body p-2">
                                                    <span>So far so good, but my plumbus doesn't work as well as
                                                        Meeseeks can't fix it, please, help me or they... They gonna
                                                        kill Morty...</span>
                                                    <span class="float-right"><small>14:16</small></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end">
                                            <div class="card message-card bg-lightblue m-1">
                                                <div class="card-body p-2">
                                                    <span>I've called Rick, I'm on the way to your house,
                                                        but probably I lost my portal gun at the party yesterday.
                                                        Anyway, don't call the other Meeseeks solve this shit.</span>
                                                    <span class="float-right mx-1"><small>14:21</small></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row justify-content-end">
                                            <div class="card message-card bg-lightblue m-1">
                                                <div class="card-body p-2">
                                                    <span>I've called Rick, I'm on the way to your house,
                                                        but probably I lost my portal gun at the party yesterday.
                                                        Anyway, don't call the other Meeseeks solve this shit.</span>
                                                    <span class="float-right mx-1"><small>14:21</small></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="input-group">
                    <input type="text" class="form-control border-0 bg-light" placeholder="Ecrire...">
                    <span class="input-group-addon ml-5">
                        <button type="button" class="btn btn-primary btn-sm">Envoyer message</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
