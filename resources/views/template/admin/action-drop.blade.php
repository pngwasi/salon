<td>
    @if (Auth::user('admin')->can('view-super'))
    <div class="dropdown">
        <button class="btn btn-info btn-sm btn-icon-split" role="button" id="dropdownMenuLink" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <span class="text-white">{{ __('Modifier') }}</span>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item" style="cursor: pointer;" onclick="event.preventDefault();
            document.getElementById('put-reject-form-{{ $id }}').submit();">Rejeter</a>

            <a class="dropdown-item" style="cursor: pointer;" onclick="event.preventDefault();
            document.getElementById('put-approved-form-{{ $id }}').submit()">Approver</a>

            <form id="put-approved-form-{{ $id }}" action="{{
        route($route, [
               'id' => $id, 'status' => 1, 'redirect' => request()->fullUrl()
            ])
        }}" method="POST" style="display: none;">
                @method('PUT')
                @csrf
            </form>
            <form id="put-reject-form-{{ $id }}" action="{{
        route($route, [
               'id' => $id, 'status' => 0, 'redirect' => request()->fullUrl()
            ])
        }}" method="POST" style="display: none;">
                @method('PUT')
                @csrf
            </form>
        </div>
    </div> 
    @else
       <span class=" badge badge-light">Action no autorisée</span> 
    @endif
</td>
