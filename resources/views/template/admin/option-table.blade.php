<td>
    <div class="dropdown">
        <button class="btn btn-info btn-sm btn-icon-split" role="button"
            id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <span class="text">{{ __('Modifier') }}</span>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item modif-users" data-id="{{ $user->id }}" onclick="event.preventDefault();" href="{{ route($routeModif, ['id' => $user->id]) }}">Modifier</a>
            <script id="modif-users-{{ $user->id }}">
                @json($user)
            </script>
            <a class="dropdown-item" href="{{ route($routeDelete, ['id' => $user->id]) }}" onclick="event.preventDefault();
                document.getElementById('delete-worker-form-{{ $user->id }}').submit();">Supprimer</a>
            <form id="delete-worker-form-{{ $user->id }}" action="{{ route($routeDelete, ['id' => $user->id, 'redirect' => request()->fullUrl()]) }}" method="POST" style="display: none;">
                @method('DELETE')
                @csrf
            </form>
        </div>
    </div>
</td>
@if (!isset($contact))
<td>
    <button class="btn btn-secondary btn-sm btn-icon-split" data-u-type="workers" data-u-email="{{ $user->email }}" data-u-phone="{{ $user->phone }}" data-u-name="{{ $user->name }}" data-u-id="{{ $user->id }}" data-toggle="modal" data-target="#contact--sb--wk" role="button">
        <span class="text">{{ __('contacter') }}</span>
    </button>
</td>
@endif
