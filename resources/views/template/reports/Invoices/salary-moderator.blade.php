@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Rapport Salaire'])
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" cellpadding="5">
                <tr>
                    <td width="65%">
                        No. : {{ $model->id }} <br />
                        Travailleur Nom : {{ $model->user->name }}  <br />
                        Travailleur Email : {{ $model->user->email }}  <br />
                        Travailleur Tel : {{ $model->user->phone }}  <br />
                    </td>
                    <td width="35%">
                        Mois : {{ $model->month }}  <br />
                        Année : {{ $model->year }}  <br />
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th rowspan="2">{{ __('Salaire') }}</th>
                        <th rowspan="2">{{ __('Avance') }}</th>
                        <th rowspan="2">{{ __('Total Salaire') }}</th>
                        <th rowspan="2">{{ __('Status') }}</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">{{ $model->id }}</td>
                        <td align="center">${{ $model->amount }}</td>
                        <td align="center">${{ $model->_advance }}</td>
                        <td align="center">${{ $model->amount - $model->_advance }}</td>
                        <td align="center">
                            @if ($model->approved)
                            <span class="badge badge-success">Apprové</span>
                            @endif
                            @if ($model->rejected)
                            <span class="badge badge-danger">Rejeté</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection