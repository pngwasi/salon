@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Rapport Sortie'])
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" cellpadding="5">
                <tr>
                    <td width="65%">
                        No. : {{ $model->id }} <br />
                        Date : {{ $model->created_at }}  <br />
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">Sr No.</th>
                        <th rowspan="2">Montant</th>
                        <th rowspan="2">Bénéficiaire</th>
                        <th rowspan="2">Motif</th>
                        <th rowspan="2">Status</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">{{ $model->id }}</td>
                        <td align="center">${{ $model->amount }}</td>
                        <td align="center">{{ $model->recipient }}</td>
                        <td align="center">{{ $model->motif }}</td>
                        <td align="center">
                            @if ($model->approved)
                            <span class="badge badge-success">Apprové</span>
                            @endif
                            @if ($model->rejected)
                            <span class="badge badge-danger">Rejeté</span>
                            @endif
                            @if (!$model->rejected && !$model->approved)
                            <span class="badge badge-warning">En attente de confirmation</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection