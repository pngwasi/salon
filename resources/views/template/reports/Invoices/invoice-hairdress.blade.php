@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Facture'])
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" cellpadding="5">
                <tr>
                    <td width="65%">
                        À,<br />
                        <b>RÉCEPTEUR (FACTURE À)</b><br />
                        Nom Client: {{ $model->subscriber ? $model->subscriber->name : '--' }} <br />
                        Adresse de facturation : {{ $model->subscriber ? $model->subscriber->address : '--' }}<br />
                    </td>
                    <td width="35%">
                        Charge inversée<br />
                        Facture No. : {{ $model->id }} <br />
                        Facture Date : {{ $model->created_at }}  <br />
                    </td>
                </tr>
            </table>
            <br />

            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">Sr No.</th>
                        <th rowspan="2">Coiffeur</th>
                        <th rowspan="2">Montant</th>
                        <th rowspan="2">Mode Paie</th>
                        <th rowspan="2">Spécification</th>
                        <th rowspan="2">Total</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="center">{{ $model->id }}</td>
                        <td align="center">{{ $model->user->name }}</td>
                        <td align="center">${{ $model->amount }}</td>
                        <td align="center">{{ $model->modePay ? $model->modePay->name: '--' }}</td>
                        <td align="center">{{ Str::ucfirst($model->specification ? $model->specification->name : '--')  }}</td>
                        <td align="center">${{ $model->amount }}</td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4"><b>Total</b></td>
                        <td align="center" colspan="4"><b>${{ $model->total ?: $model->amount }}</b></td>
                    </tr>
                </tbody>
            </table>
            <br /><br /><br />
            <p align="right">----------------------------------------<br />Signature du destinataire</p>
        </td>
    </tr>
</table>
@endsection