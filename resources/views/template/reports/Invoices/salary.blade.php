@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Rapport Salaire'])
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" cellpadding="5">
                <tr>
                    <td width="65%">
                        No. : {{ $model->_id }} <br />
                        Travailleur Nom : {{ $model->_name }}  <br />
                        Travailleur Email : {{ $model->_email }}  <br />
                        Travailleur Tel : {{ $model->_phone }}  <br />
                    </td>
                    <td width="35%">
                        Mois : {{ $model->month }}  <br />
                        Année : {{ $model->year }}  <br />
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th rowspan="2">{{ __('Salaire') }} (40%)</th>
                        <th rowspan="2">{{ __('Entré') }}</th>
                        <th rowspan="2">{{ __('Bonus') }}</th>
                        <th rowspan="2">{{ __('Avance') }}</th>
                        <th rowspan="2">{{ __('Total Salaire') }}</th>
                        <th rowspan="2">{{ __('Status') }}</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    <tr>
                        <th align="center" scope="row">{{ $model->_id }}</th>
                        <td align="center">${{ $model->_salary }} (40%)</td>
                        <td align="center">${{ $model->_hairdress }}</td>
                        <td align="center">${{ $model->_bonus }}</td>
                        <td align="center">${{ $model->_advance }}</td>
                        <td align="center">${{ $model->_salary - $model->_advance }}</td>
                        <td align="center">
                            @if ($model->_salaryStatus > 0)
                            <span class="badge badge-success">Apprové</span>
                            @else
                            <span class="badge badge-secondary">No Apprové</span>
                            @endif
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection