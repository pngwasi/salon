@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Rapport'])
    </tr>
    <tr>
        <td colspan="2">
            <table width="100%" cellpadding="5">
                <tr>
                    <td width="35%">
                        Années: {{ $toYears }}<br />
                        ID : {{ time() }}<br />
                    </td>
                </tr>
            </table>
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th rowspan="2">Année</th>
                        <th colspan="2">Entrés</th>
                        <th colspan="2">Sorties</th>
                        <th rowspan="2">Travailleurs</th>
                        <th rowspan="2">Abonnés</th>
                        <th rowspan="2">Materiel</th>
                    </tr>
                    <tr>
                        <th>Approvés</th>
                        <th>Réjetés</th>
                        <th>Approvés</th>
                        <th>Réjetés</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($reports as $key => $report)
                        <tr>
                            <td>{{ $key }}</td>
                            <td>{{ $report['year'] }}</td>
                            <td>${{ $report['entry']['approved'] }}</td>
                            <td>${{ $report['entry']['rejected'] }}</td>
                            <td>${{ $report['disburse']['approved'] }}</td>
                            <td>${{ $report['disburse']['rejected'] }}</td>
                            <td>{{ $report['users'] }}</td>
                            <td>{{ $report['subscribers'] }}</td>
                            <td>{{ $report['materials'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection