@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Liste Travailler'])
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th rowspan="2">Nom</th>
                        <th rowspan="2">Email</th>
                        <th rowspan="2">Tel</th>
                        <th rowspan="2">Adresse</th>
                        <th rowspan="2">Profile</th>
                        <th rowspan="2">Status</th>
                        <th rowspan="2">Ajouté le</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td align="center">{{ $user->id }}</td>
                            <td align="center">{{ $user->name }}</td>
                            <td align="center">{{ $user->email }}</td>
                            <td align="center">{{ $user->phone }}</td>
                            <td align="center">{{ $user->address }}</td>
                            <td>
                                {{ $user->moderator ? 'Moderateur, ': '' }}
                                {{ $user->cashier ? 'Caissier': '' }}
                                {{ !$user->moderator && !$user->cashier ? 'Coiffeur': '' }}
                            </td>
                            <td>
                                {{ $user->active ? 'Actif': 'Inactif' }}
                            </td>
                            <td align="center">{{ $user->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection

