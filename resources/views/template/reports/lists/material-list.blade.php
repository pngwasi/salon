@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Liste Materiel'])
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th rowspan="2">Materiel</th>
                        <th rowspan="2">Actif</th>
                        <th rowspan="2">Inactif</th>
                        <th rowspan="2">Prix U</th>
                        <th rowspan="2">Status</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    @foreach ($materials as $material)
                        <tr>
                            <td align="center">{{ $material->id }}</td>
                            <td align="center">{{ $material->name }}</td>
                            <td align="center">{{ $material->stock()->where('status', true)->count() }}</td>
                            <td align="center">{{ $material->stock()->where('status', false)->count() }}</td>
                            <td align="center">${{ $material->price }}</td>
                            <td align="center">
                                {{ $material->active ? 'Actif': 'Inactif' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection

