@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Liste Spécification'])
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th rowspan="2">Spécification</th>
                        <th rowspan="2">Tax</th>
                        <th rowspan="2">Montant</th>
                        <th rowspan="2">Status</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    @foreach ($specifications as $specification)
                        <tr>
                            <td align="center">{{ $specification->id }}</td>
                            <td align="center">{{ $specification->name }}</td>
                            <td align="center">${{ $specification->tax }}</td>
                            <td align="center">${{ $specification->amount }}</td>
                            <td align="center">
                                {{ $specification->active ? 'Actif': 'Inactif' }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection

