@extends('template.reports.layout.report-layout')

@section('report')
<table width="100%" border="1" cellpadding="5" cellspacing="0">
    <tr>
        @include('template.reports.layout.title', ['title' => 'Liste Abonnés'])
    </tr>
    <tr>
        <td colspan="2">
            <br />
            <table width="100%" border="1" cellpadding="5" cellspacing="0">
                <thead>
                    <tr>
                        <th rowspan="2">No.</th>
                        <th rowspan="2">Nom</th>
                        <th rowspan="2">Email</th>
                        <th rowspan="2">Tel</th>
                        <th rowspan="2">Adresse</th>
                        <th rowspan="2">Status</th>
                        <th rowspan="2">Ajouté le</th>
                    </tr>
                    <tr></tr>
                </thead>
                <tbody>
                    @foreach ($subscribers as $subscriber)
                        <tr>
                            <td align="center">{{ $subscriber->id }}</td>
                            <td align="center">{{ $subscriber->name }}</td>
                            <td align="center">{{ $subscriber->email }}</td>
                            <td align="center">{{ $subscriber->phone }}</td>
                            <td align="center">{{ $subscriber->address }}</td>
                            <td>
                                {{ $subscriber->active ? 'Actif': 'Inactif' }}
                            </td>
                            <td align="center">{{ $subscriber->created_at }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <br />
        </td>
    </tr>
</table>
@endsection
