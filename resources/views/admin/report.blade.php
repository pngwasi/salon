@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Rapports') }}</h1>
</div>
<div data-controller="Admin-Report" class="my-5">
    <div class="row justify-content-center">
        <div class="col-md-6 col-12">
            <div class="card">
                <div class="card-header"><small>Repport</small></div>
                <div class="card-body">
                    <form action="{{ route('admin.report.generate') }}" data-action="Admin-Report#report" method="GET" class="form">
                        <ul>
                            <ol>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="report-by-select" name="report-select"
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="report-by-select">Rapport par
                                        Selection</label>
                                </div>
                                <ul>
                                    <ol>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" disabled name="list-workers" class="custom-control-input" id="list-workers">
                                            <label class="custom-control-label" for="list-workers">Liste
                                                Travaillleurs</label>
                                        </div>
                                    </ol>
                                    <ol>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" disabled name="list-subscriber" class="custom-control-input" id="list-subscriber">
                                            <label class="custom-control-label" for="list-subscriber">Liste
                                                Abonné</label>
                                        </div>
                                    </ol>
                                    <ol>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" disabled name="list-material" class="custom-control-input" id="list-material">
                                            <label class="custom-control-label" for="list-material">Liste
                                                Materiel</label>
                                        </div>
                                    </ol>
                                    <ol>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" disabled name="list-specification" class="custom-control-input" id="list-specification">
                                            <label class="custom-control-label" for="list-specification">Liste
                                                Spécification</label>
                                        </div>
                                    </ol>
                                </ul>
                            </ol>
                            <ol>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="general-report" checked name="report-select"
                                        class="custom-control-input">
                                    <label class="custom-control-label" for="general-report">Rapport general</label>
                                </div>
                            </ol>
                        </ul>
                        <button type="submit" class="btn btn-sm btn-primary">Generer Rapport</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
