@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Abonnés') }}</h1>
</div>
<div data-controller="Subscribers">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.subscribers') ? 'active' : '' }}" href="{{ route('admin.subscribers') }}">{{ __('Abonnés') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.subscriptions') ? 'active' : '' }}" href="{{ route('admin.subscriptions') }}">{{ __('Abonnements') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.subscriber.profile') ? 'active' : '' }}" href="{{ route('admin.subscriber.profile') }}">{{ __('Profile') }}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('subscribers')
        </div>
    </div>
</div>

@endsection
