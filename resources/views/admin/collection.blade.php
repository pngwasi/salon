@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Entrés') }}</h1>
</div>
<div data-controller="Admin-Collect">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.collection.hairdressing') ? 'active' : '' }}" 
                    href="{{ route('admin.collection.hairdressing') }}">{{ __('coiffure') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.collection.bonus') ? 'active' : '' }}"
                        href="{{ route('admin.collection.bonus') }}">{{ __('Bonus') }}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('collection')
        </div>
    </div>
</div>

@endsection
