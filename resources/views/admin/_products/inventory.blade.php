@extends('admin.products')

@section('products')
<div>
    <div class="row justify-content-center">
        @forelse ($materials as $material)
        <div class="col-md-6 col-lg-3">
            <div class="card shadow border-light">
                <div class="card-header text-center">
                    <h4>{{ $material->name }}</h4>
                </div>
                <div class="card-body">
                    <table class="table m-0 text-muted">
                        <thead>
                            <tr>
                                <th scope="col">Actif</th>
                                <th scope="col">Inactif</th>
                                <th scope="col">status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="badge badge-success">
                                        {{ $material->stock()->where('status', true)->count() }}
                                    </span></td>
                                <td><span class="badge badge-warning">
                                        {{ $material->stock()->where('status', false)->count() }}
                                    </span></td>
                                <td>
                                    @if ($material->status)
                                    <span class="badge badge-success">Actif</span>
                                    @else
                                    <span class="badge badge-danger">Inactif</span>
                                    @endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table m-0 text-muted">
                        <thead>
                            <tr>
                                <th scope="col">Total</th>
                                <th scope="col">Prix U</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="badge badge-info text-white">
                                        {{ $material->stock->count() }}
                                    </span></td>
                                <td><span class="badge badge-info text-white">
                                        ${{ $material->price }}
                                    </span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer text-muted">
                    <a  class="btn btn-info btn-sm btn-icon-split  modif-users"
                        data-id="{{ $material->id }}" onclick="event.preventDefault();"
                        href="{{ route('admin.modifyProduct', ['id' => $material->id]) }}">Modifier</a>
                    <script id="modif-users-{{ $material->id }}">
                        @json($material)
                    </script>
                    <a class="btn btn-danger btn-sm btn-icon-split" href="{{ route('admin.deleteProduct', ['id' => $material->id]) }}" onclick="event.preventDefault();
                    document.getElementById('delete-form-{{ $material->id }}').submit();">Supprimer</a>
                    <form id="delete-form-{{ $material->id }}" action="{{ route('admin.deleteProduct', ['id' => $material->id, 'redirect' => request()->fullUrl()]) }}" method="POST" style="display: none;">
                        @method('DELETE')
                        @csrf
                    </form>
                </div>
            </div>
        </div>
        @empty
        <div class="card">
            <div class="card-body">
                <h4>Aucun resultat</h4>
            </div>
        </div>
        @endforelse
    </div>
    <div class="justify-content-center">
        {{ $materials->links() }}
    </div>
</div>

<x-user-modify>
<div class="form-row">
    <div class="col-md-12 mb-3">
        <label for="name">{{ __('Nom') }}</label>
        <input type="text" class="form-control" name="name" id="name" required>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="price">{{ __('Prix Unitaire') }}</label>
        <input type="text" name="price" value="0" class="form-control" id="price" required>
    </div>
    <div class="col-md-6 mb-3">
        <label for="stock">{{ __('Stock') }}</label>
        <input type="text" class="form-control" readonly value="0" name="activeStock" id="stock">
    </div>
</div>

<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="m-price">{{ __('Inatif Stock') }}</label>
        <input type="text" name="inactif_stock" value="0" class="form-control" id="m-price" required>
    </div>
    <div class="col-md-6 mb-3">
        <label for="m-stock">{{ __('Ajouter Stock') }}</label>
        <input type="text" class="form-control" value="0" name="add_stock" id="m-stock" required>
    </div>
</div>
</x-user-modify>

@endsection
