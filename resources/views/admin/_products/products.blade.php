@extends('admin.products')

@section('products')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header">
            <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal" data-target="#newMaterial">
                <span class="text">{{ __('Nouveau Materiel') }}</span>
            </button>
        </div>
        <div class="card-body">
            <table class="table m-0 text-muted">
                <thead>
                    <tr>
                        <th scope="col">Material</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($stock as $sk)
                    <tr>
                        <td>{{ $sk->material->name }}</td>
                        <td><span class="badge badge-info text-white">{{ $sk->total_stock }}</span></td>
                        <td>{{ $sk->date }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="newMaterial" tabindex="-1" role="dialog" aria-labelledby="newMaterialTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newMaterialTitle">{{ __('Ajouter Materiel') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" autocomplete="off" aria-autocomplete="none" method="POST" action="{{ route('admin.newMaterial') }}"
                data-action="Products#newMaterial">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="name">{{ __('Nom') }}</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="price">{{ __('Prix Unitaire') }}</label>
                            <input type="text" name="price" value="0" class="form-control" id="price" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="stock">{{ __('Stock') }}</label>
                            <input type="text" class="form-control" value="0" name="stock" id="stock" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
