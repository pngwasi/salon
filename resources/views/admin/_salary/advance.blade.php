@extends('admin.salary')

@section('salary')
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('admin.salary.advance') ? 'active' : '' }}"  href="{{ route('admin.salary.advance') }}">{{ __('Coiffeurs') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('admin.salary.advance.moderators') ? 'active' : '' }}"  href="{{ route('admin.salary.advance.moderators') }}">{{ __('Moderateurs') }}</a>
            </li>
        </ul>
        <div class="content mt-3">
            @yield('advance')
        </div>
    </div>
</div>
@endsection
