@extends('admin.salary')

@section('salary')
<div class="justify-content-center">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Email') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>

                            <th scope="col">{{ __('Montant Salaire') }} (40%)</th>
                            <th scope="col">{{ __('Montant Entré') }}</th>
                            <th scope="col">{{ __('Bonus') }}</th>
                            <th scope="col">{{ __('Avance Salaire') }}</th>
                            <th scope="col">{{ __('Total Salaire') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                            <th scope="col">{{ __('Imprimer') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>

                            <td>${{ $user->_salary }} (40%)</td>
                            <td>${{ $user->_hairdress }}</td>
                            <td>${{ $user->_bonus }}</td>
                            <td>${{ $user->_advance }}</td>
                            <td>${{ $user->_salary - $user->_advance }}</td>
                            <td>
                                @if ($user->_salaryStatus > 0)
                                <span class="badge badge-success">Apprové</span>
                                @else
                                <span class="badge badge-secondary">No Apprové</span>
                                @endif
                            </td>
                            @include('template.admin.action-drop', [
                                'route' => 'admin.salary.approved-now-date',
                                'id' => $user->id
                            ])
                            <td>
                                <button type="button" class="btn btn-light btn-sm btn-icon-split report-modal"
                                    data-url="{{ route('admin.report.salary', [
                                        'uid' => $user->id,
                                        'year' => now()->format('Y'),
                                        'month' => now()->format('m'),
                                    ]) }}">Imprimer</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $users->links() }}
</div>
@endsection
