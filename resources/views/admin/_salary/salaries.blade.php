@extends('admin.salary')

@section('salary')
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('admin.salary.salaries') ? 'active' : '' }}"  href="{{ route('admin.salary.salaries') }}">{{ __('Coiffeurs') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('admin.salary.salaries.moderators') ? 'active' : '' }}"  href="{{ route('admin.salary.salaries.moderators') }}">{{ __('Moderateurs') }}</a>
            </li>
        </ul>
        <div class="content mt-3">
            @yield('salaries')
        </div>
    </div>
</div>
@endsection
