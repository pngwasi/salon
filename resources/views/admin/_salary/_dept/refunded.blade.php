@extends('admin._salary.dept')

@section('dept')
<div class="justify-content-center">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>
                            <th scope="col">{{ __('Montant') }}</th>
                            <th scope="col">{{ __('Remboursé') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Date') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($depts as $dept)
                        <tr>
                            <th scope="row">{{ $dept->id }}</th>
                            <td>{{ $dept->user->name }}</td>
                            <td>{{ $dept->user->phone }}</td>
                            <td>{{ $dept->amount }}</td>
                            <td>
                                @if ($dept->refunded)
                                <span class="badge badge-success">Remboursé</span>
                                @else
                                <span class="badge warning-danger">Dette</span>
                                @endif
                            </td>
                            <td>
                                @if ($dept->approved)
                                <span class="badge badge-success">Apprové</span>
                                @endif
                                @if ($dept->rejected)
                                <span class="badge badge-danger">Rejeté</span>
                                @endif
                            </td>
                            <td>{{ $dept->created_at }}</td>
                            @include('template.admin.action-drop', [
                                'route' => 'admin.salary.dept.approved',
                                'id' => $dept->id
                            ])
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $depts->links() }}
</div>
@endsection