@extends('admin._salary.dept')

@section('dept')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header">
            <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal" data-target="#newDept">
                <span class="text">{{ __('Ajouter Dette') }}</span>
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>
                            <th scope="col">{{ __('Montant') }}</th>
                            <th scope="col">{{ __('Remboursé') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Date') }}</th>
                            <th scope="col">{{ __('Rembourser') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($depts as $dept)
                        <tr>
                            <th scope="row">{{ $dept->id }}</th>
                            <td>{{ $dept->user->name }}</td>
                            <td>{{ $dept->user->phone }}</td>
                            <td>{{ $dept->amount }}</td>
                            <td>
                                @if ($dept->refunded)
                                <span class="badge badge-success">Remboursé</span>
                                @else
                                <span class="badge badge-warning">Dette</span>
                                @endif
                            </td>
                            <td>
                                @if ($dept->approved)
                                <span class="badge badge-success">Apprové</span>
                                @endif
                                @if ($dept->rejected)
                                <span class="badge badge-danger">Rejeté</span>
                                @endif
                            </td>
                            <td>{{ $dept->created_at }}</td>
                            <td>
                                @if (!$dept->refunded)
                                    <a href="{{ route('admin.salary.dept.refunded-action', [
                                        'id' => $dept->id,
                                        'redirect' => request()->fullUrl()
                                    ]) }}" class="btn btn-info btn-sm btn-icon-split">Rembourser</button>
                                @else
                                    --
                                @endif
                            </td>
                            @include('template.admin.action-drop', [
                                'route' => 'admin.salary.dept.approved',
                                'id' => $dept->id
                            ])
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $depts->links() }}
</div>

<div class="modal fade" id="newDept" tabindex="-1" role="dialog" aria-labelledby="newDeptTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newDeptTitle">{{ __('Dette') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" autocomplete="off" aria-autocomplete="none" method="POST" action="{{ route('admin.salary.dept.newDept') }}"
                data-action="Admin-Salary#newDept">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="uid">
                        <div class="col-md-6 col-12 mb-3">
                            <div>
                                <button type="button" class="btn  btn-block btn-outline-info dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Travailleur
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.worker') }}"
                                                data-target="Admin-Salary.hlsS"
                                                class="form-control bg-light border-0 small" placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="Admin-Salary.hlsSList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mb-3">
                            <input type="text" class="form-control" id="worker_nameS"  readonly
                                placeholder="Travailleur (optionnel)">
                            <div class="valid-feedback"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="amount">{{ __('Montant') }}</label>
                            <input type="text" name="montant" value="0" class="form-control" id="amount" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection