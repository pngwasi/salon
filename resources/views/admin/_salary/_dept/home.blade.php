@extends('admin._salary.dept')

@section('dept')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <form method="GET" class="d-inline-block" action="" autocomplete="off">
                <input type="hidden" name="uid" value="{{ request('uid') }}">
                <div class="form-row">
                    <div class="col-lg-5 col-md-12 col-sm-12 mb-3">
                        <div>
                            <button type="button" class="btn  btn-block btn-outline-info dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Recherche Travailleur
                            </button>
                            <div class="dropdown-menu shadow">
                                <div class="mx-1">
                                    <div class="input-group">
                                        <input type="text" data-url="{{ route('search.worker') }}"
                                            data-target="Admin-Salary.hlsS"
                                            class="form-control bg-light border-0 small" placeholder="Recherche...">
                                    </div>
                                </div>
                                <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                <div data-target="Admin-Salary.hlsSList"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12 mb-3">
                        <input type="text" class="form-control" id="worker_nameS" value="{{ $us ? $us->name : '' }}" readonly
                            placeholder="Travailleur (optionnel)">
                        <div class="valid-feedback"></div>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12 mb-3">
                        <button type="submit" class="btn btn-primary">Recherche</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>
                            <th scope="col">{{ __('Montant') }}</th>
                            <th scope="col">{{ __('Remboursé') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Date') }}</th>
                            <th scope="col">{{ __('Rembourser') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($depts as $dept)
                        <tr>
                            <th scope="row">{{ $dept->id }}</th>
                            <td>{{ $dept->user->name }}</td>
                            <td>{{ $dept->user->phone }}</td>
                            <td>{{ $dept->amount }}</td>
                            <td>
                                @if ($dept->refunded)
                                <span class="badge badge-success">Remboursé</span>
                                @else
                                <span class="badge badge-warning">Dette</span>
                                @endif
                            </td>
                            <td>
                                @if ($dept->approved)
                                <span class="badge badge-success">Apprové</span>
                                @endif
                                @if ($dept->rejected)
                                <span class="badge badge-danger">Rejeté</span>
                                @endif
                            </td>
                            <td>{{ $dept->created_at }}</td>
                            <td>
                            @if (!$dept->refunded)
                                <a href="{{ route('admin.salary.dept.refunded-action', [
                                    'id' => $dept->id,
                                    'redirect' => request()->fullUrl()
                                ]) }}" class="btn btn-info btn-sm btn-icon-split">Rembourser</button>
                            @else
                                --
                            @endif
                            </td>
                            @include('template.admin.action-drop', [
                                'route' => 'admin.salary.dept.approved',
                                'id' => $dept->id
                            ])
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $depts->links() }}
</div>
@endsection