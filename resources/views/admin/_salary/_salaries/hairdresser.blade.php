@extends('admin._salary.salaries')

@section('salaries')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header d-flex justify-content-between">
            <form method="GET" class="d-inline-block" action="" autocomplete="off">
                <input type="hidden" name="uid" value="{{ request('uid') }}">
                <div class="form-row">
                    <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                        <div>
                            <button type="button" class="btn  btn-block btn-outline-info dropdown-toggle"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Recherche Travailleur
                            </button>
                            <div class="dropdown-menu shadow">
                                <div class="mx-1">
                                    <div class="input-group">
                                        <input type="text" data-url="{{ route('search.worker') }}"
                                            data-target="Admin-Salary.hlsS"
                                            class="form-control bg-light border-0 small" placeholder="Recherche...">
                                    </div>
                                </div>
                                <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                <div data-target="Admin-Salary.hlsSList"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                        <input type="text" class="form-control" id="worker_nameS" value="{{ $us ? $us->name : '' }}" readonly
                            placeholder="Travailleur (optionnel)">
                        <div class="valid-feedback"></div>
                    </div>
                    <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                        <input type="number" class="form-control" name="month" value="{{ request('month') }}"
                            placeholder="Mois (optionnel)">
                    </div>
                    <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                        <input type="number" class="form-control" value="{{ request('year') }}" name="year"
                            placeholder="Year">
                    </div>
                    <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                        <button type="submit" class="btn btn-primary">Recherche</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>
                            <th scope="col">{{ __('Salaire') }} (40%)</th>
                            <th scope="col">{{ __('Entré') }}</th>
                            <th scope="col">{{ __('Bonus') }}</th>
                            <th scope="col">{{ __('Avance') }}</th>
                            <th scope="col">{{ __('Total Salaire') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Mois') }}</th>
                            <th scope="col">{{ __('Année') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                            <th scope="col">{{ __('Imprimer') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $user->_id }}</th>
                            <td>{{ $user->_name }}</td>
                            <td>{{ $user->_phone }}</td>

                            <td>${{ $user->_salary }} (40%)</td>
                            <td>${{ $user->_hairdress }}</td>
                            <td>${{ $user->_bonus }}</td>
                            <td>${{ $user->_advance }}</td>
                            <td>${{ $user->_salary - $user->_advance }}</td>
                            <td>
                                @if ($user->_salaryStatus > 0)
                                <span class="badge badge-success">Apprové</span>
                                @else
                                <span class="badge badge-secondary">No Apprové</span>
                                @endif
                            </td>
                            <td>{{ $user->month }}</td>
                            <td>{{ $user->year }}</td>
                            @include('template.admin.action-drop', [
                                'route' => 'admin.salary.approved-now-date',
                                'id' => $user->_id
                            ])
                            <td>
                                <button type="button" class="btn btn-light btn-sm btn-icon-split report-modal"
                                    data-url="{{ route('admin.report.salary', [
                                        'uid' => $user->_id,
                                        'year' => $user->year,
                                        'month' => $user->month,
                                    ]) }}">Imprimer</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $users->links() }}
</div>
@endsection
