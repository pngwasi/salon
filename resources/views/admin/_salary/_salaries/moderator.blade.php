@extends('admin._salary.salaries')

@section('salaries')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header">
            <div class="mb-3">
            <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal" data-target="#newSalary">
                <span class="text">{{ __('Ajouter Salaire') }}</span>
            </button>
            <small>( <strong>Année: {{ now()->format('Y') }}, Mois: {{ now()->format('m') }}</strong> )</small>
            </div>
            <div class="d-flex justify-content-between">
                <form method="GET" class="d-inline-block" action="" autocomplete="off">
                    <input type="hidden" name="uid" value="{{ request('uid') }}">
                    <div class="form-row">
                        <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                            <div>
                                <button type="button" class="btn  btn-block btn-outline-info dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Travailleur
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.worker') }}"
                                                data-target="Admin-Salary.hlsS"
                                                class="form-control bg-light border-0 small" placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="Admin-Salary.hlsSList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <input type="text" class="form-control" id="worker_nameS" value="{{ $us ? $us->name : '' }}" readonly
                                placeholder="Travailleur (optionnel)">
                            <div class="valid-feedback"></div>
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <input type="number" class="form-control" name="month" value="{{ request('month') }}"
                                placeholder="Mois (optionnel)">
                        </div>
                        <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                            <input type="number" class="form-control" value="{{ request('year') }}" name="year"
                                placeholder="Year">
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <button type="submit" class="btn btn-primary">Recherche</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>
                            <th scope="col">{{ __('Salaire') }}</th>
                            <th scope="col">{{ __('Avance') }}</th>
                            <th scope="col">{{ __('Total Salaire') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Mois') }}</th>
                            <th scope="col">{{ __('Année') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                            <th scope="col">{{ __('Imprimer') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($salaries as $salary)
                        <tr>
                            <th scope="row">{{ $salary->id }}</th>
                            <td>{{ $salary->user->name }}</td>
                            <td>{{ $salary->user->phone }}</td>

                            <td>${{ $salary->amount }}</td>
                            <td>${{ $salary->_advance }}</td>
                            <td>${{ $salary->amount - $salary->_advance }}</td>
                            <td>
                                @if ($salary->approved)
                                <span class="badge badge-success">Apprové</span>
                                @endif
                                @if ($salary->rejected)
                                <span class="badge badge-danger">Rejeté</span>
                                @endif
                            </td>
                            <td>{{ $salary->month }}</td>
                            <td>{{ $salary->year }}</td>
                            @include('template.admin.action-drop', [
                                'route' => 'admin.salary.salaries.moderators-approved',
                                'id' => $salary->id
                            ])
                            <td>
                                <button type="button" class="btn btn-light btn-sm btn-icon-split report-modal"
                                    data-url="{{ route('admin.report.salary-moderator', [
                                        'id' => $salary->id,
                                        'year' => $salary->year,
                                        'month' => $salary->month,
                                    ]) }}">Imprimer</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $salaries->links() }}
</div>

<div class="modal fade" id="newSalary" data-controller="Admin-SalaryModerator" tabindex="-1" role="dialog" aria-labelledby="newSalaryTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newSalaryTitle">{{ __('Avance') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" autocomplete="off" aria-autocomplete="none" method="POST"
                action="{{ route('admin.salary.salaries.moderators-newSalary') }}"
                data-action="Admin-SalaryModerator#newSalary">
                <div class="modal-body">
                    <div class="form-row">
                        <input type="hidden" name="uid">
                        <div class="col-md-6 col-12 mb-3">
                            <div>
                                <button type="button" class="btn  btn-block btn-outline-info dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Travailleur
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.worker') }}"
                                                data-target="Admin-SalaryModerator.hls"
                                                class="form-control bg-light border-0 small" placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="Admin-SalaryModerator.hlsList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12 mb-3">
                            <input type="text" class="form-control" id="worker_name" readonly
                                placeholder="Travailleur (optionnel)">
                            <div class="valid-feedback"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="amount">{{ __('Montant') }}</label>
                            <input type="text" name="montant" value="0" class="form-control" id="amount" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>   

@endsection
