@extends('admin.salary')

@section('salary')
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('admin.salary.dept') ? 'active' : '' }}"  href="{{ route('admin.salary.dept') }}">{{ __('Acceuil') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('admin.salary.dept.refunded') ? 'active' : '' }}"  href="{{ route('admin.salary.dept.refunded') }}">{{ __('Remboursés') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('admin.salary.dept.no-refunded') ? 'active' : '' }}"  href="{{ route('admin.salary.dept.no-refunded') }}">{{ __('Dette') }}</a>
            </li>
        </ul>
        <div class="content mt-3">
            @yield('dept')
        </div>
    </div>
</div>
@endsection
