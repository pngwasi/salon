@extends('admin.user-admin')

@section('user-admin')
<div class="mb-4">
    <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal" data-target="#newAdminstrateur">
        <span class="text">{{ __('Nouveau Administrateur') }}</span>
    </button>
</div>
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h5><span class="badge badge-info">Super Administrateur</span></h5>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{ __('Email') }}</th>
                                <th scope="col">{{ __('Status') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($supers as $super)
                            <tr>
                                <th scope="row">{{ $super->id }}</th>
                                <td>{{ $super->email }}</td>
                                <td>
                                    @if($super->active)
                                    <span class="badge badge-success">Actif</span>
                                    @else
                                    <span class="badge badge-danger">Inactif</span>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card-header">
            <h5><span class="badge badge-secondary">Administrateur</span></h5>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Email') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                            <th scope="col">{{ __('Contact') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($admins as $admin)
                        <tr>
                            <th scope="row">{{ $admin->id }}</th>
                            <td>{{ $admin->email }}</td>
                            <td>
                                @if($admin->active)
                                <span class="badge badge-success">Actif</span>
                                @else
                                <span class="badge badge-danger">Inactif</span>
                                @endif
                            </td>
                            @include('template.admin.option-table',
                            [
                            'user' => $admin,
                            'routeModif' => 'admin.user.settings.admin.modify',
                            'routeDelete' => 'admin.user.settings.admin.delete'
                            ])
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<x-user-modify>
<div class="form-row">
    <div class="col-md-12 mb-3">
        <label for="d-email">Email</label>
        <input type="email" class="form-control" name="email" id="d-email"
            required>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="d-password">Nouveau mot de passe</label>
        <input type="password" class="form-control" name="password" id="d-password">
    </div>
    <div class="col-md-6 mb-3">
        <label for="d-password_confirmation">Confirmer mot de passe</label>
        <input type="password" class="form-control" name="password_confirmation" id="d-password_confirmation">
    </div>
</div>
</x-user-modify>

<div class="modal fade" id="newAdminstrateur" tabindex="-1" role="dialog" aria-labelledby="newTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newTitle">{{ __('Ajouter Administrateur') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.user.settings.newAdmin') }}"
                data-action="UserAdmin#newAdminstrateur">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="password">Nouveau mot de passe</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="password_confirmation">Confirmer mot de passe</label>
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div> 
@endsection
