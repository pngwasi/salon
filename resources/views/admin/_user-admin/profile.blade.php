@extends('admin.user-admin')

@section('user-admin')
<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header"><h4>Modifier Profile</h4> 
                <span class="badge badge-info">{{ Auth::user('admin')->super ? 'Super Administrateur': 'Administrateur' }}</span>
            </div>
            <div class="card-body">
                <form action="{{ route('admin.user.modify-profile') }}" data-action="UserAdmin#modifyProfile" autocomplete="off" method="post">
                    @method('PUT')
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" value="{{ Auth::user('admin')->email }}" name="email" id="email" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="password">Nouveau mot de passe</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="password_confirmation">Confirmer mot de passe</label>
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                        </div>
                    </div>
                    <div class="form-row">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection