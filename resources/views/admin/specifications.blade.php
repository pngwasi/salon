@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Specifications') }}</h1>
</div>
<div data-controller="Specifications">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.specifications') ? 'active' : '' }}" href="{{ route('admin.specifications') }}">{{ __('Specifications') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.payType') ? 'active' : '' }}" href="{{ route('admin.payType') }}">{{ __('Mode Paie') }}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('specifications')
        </div>
    </div>
</div>

@endsection
