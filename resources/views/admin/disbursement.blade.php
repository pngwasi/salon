@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Sorties') }}</h1>
</div>
<div data-controller="Disbursement">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.disbursement') ? 'active' : '' }}" 
                    href="{{ route('admin.disbursement') }}">{{ __('Décaissement') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.disbursement.confirmed') ? 'active' : '' }}"
                        href="{{ route('admin.disbursement.confirmed') }}">{{ __('Confirmés') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.disbursement.rejected') ? 'active' : '' }}"
                        href="{{ route('admin.disbursement.rejected') }}">{{ __('Rejetés') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.disbursement.request') ? 'active' : '' }}"
                        href="{{ route('admin.disbursement.request') }}">{{ __('Confirmation') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.disbursement.inventory') ? 'active' : '' }}"
                        href="{{ route('admin.disbursement.inventory') }}">{{ __('Inventaire') }}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('disbursement')
        </div>
    </div>
</div>

@endsection
