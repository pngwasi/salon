<div class="justify-content-center">
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Montant</th>
                    <th scope="col">Bénéficiaire</th>
                    <th scope="col">Motif</th>
                    <th scope="col">Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Modifier</th>
                    <th scope="col">Imprimer</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($disbursements as $disbursement)
                <tr>
                    <td>{{ $disbursement->id }}</td>
                    <td><span class="badge badge-info text-white">${{ $disbursement->amount }}</span></td>
                    <td>{{ $disbursement->recipient }}</td>
                    <td><small>{{ $disbursement->motif }}</small></td>
                    <td>{{ $disbursement->created_at }}</td>
                    <td>
                        @if ($disbursement->approved)
                        <span class="badge badge-success">Apprové</span>
                        @endif
                        @if ($disbursement->rejected)
                        <span class="badge badge-danger">Rejeté</span>
                        @endif
                        @if (!$disbursement->rejected && !$disbursement->approved)
                        <span class="badge badge-warning">En attente de confirmation</span>
                        @endif
                    </td>
                    @include('template.admin.action-drop', [
                        'route' => 'admin.disbursement.approved',
                        'id' => $disbursement->id
                    ])
                    <td>
                        <button type="button" class="btn btn-light btn-sm btn-icon-split report-modal"
                            data-url="{{ route('admin.report.disburse', ['id' => $disbursement->id], false) }}">Imprimer</button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $disbursements->links() }}
</div>