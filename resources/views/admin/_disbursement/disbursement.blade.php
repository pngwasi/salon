@extends('admin.disbursement')

@section('disbursement')
<form method="GET" action="" autocomplete="off">
    <div class="form-row">
        <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
            <input type="number" class="form-control" name="day" value="{{ request('day') }}"
                placeholder="Jour (optionnel)">
        </div>
        <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
            <input type="number" class="form-control" name="month" value="{{ request('month') }}"
                placeholder="Mois (optionnel)">
        </div>
        <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
            <input type="number" class="form-control" value="{{ request('year') }}" name="year"
                placeholder="Year">
        </div>
        <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
            <button type="submit" class="btn btn-primary">recherche</button>
        </div>
    </div>
</form>
@include('admin._disbursement._disburse-table', ['disbursements' => $disbursements])
@endsection
