@extends('admin.disbursement')

@section('disbursement')
@include('admin._disbursement._disburse-table', ['disbursements' => $disbursements])
@endsection