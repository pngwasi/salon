@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Materiels') }}</h1>
</div>
<div data-controller="Products">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.products.inventory') ? 'active' : '' }}" 
                    href="{{ route('admin.products.inventory') }}">{{ __('Inventaire') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.products') ? 'active' : '' }}"
                        href="{{ route('admin.products') }}">{{ __('Materiels') }}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('products')
        </div>
    </div>
</div>

@endsection
