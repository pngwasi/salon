@extends('admin.specifications')

@section('specifications')
<div class="justify-content-center">
<div class="card">
    <div class="card-header">
        <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal"
            data-target="#newSpecification">
            <span class="text">{{ __('Nouvelle Specification') }}</span>
        </button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">{{ __('Nom') }}</th>
                        <th scope="col">{{ __('Tax') }}</th>
                        <th scope="col">{{ __('Montant') }}</th>
                        <th scope="col">{{ __('Status') }}</th>
                        <th scope="col">{{ __('Modifier') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($specifications as $specification)
                    <tr>
                        <th scope="row">{{ $specification->id }}</th>
                        <td>{{ $specification->name }}</td>
                        <th scope="row">${{ $specification->tax }}</th>
                        <th scope="row">${{ $specification->amount }}</th>
                        <td>
                            @if($specification->active)
                            <span class="badge badge-success">Actif</span>
                            @else
                            <span class="badge badge-danger">Inactif</span>
                            @endif
                        </td>
                        @include('template.admin.option-table',
                            ['user' => $specification, 'contact' => true, 'routeModif' => 'admin.modifySpecification', 'routeDelete' => 'admin.deleteSpecification'])
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
{{ $specifications->links() }}
</div>

<div class="modal fade" id="newSpecification" tabindex="-1" role="dialog" aria-labelledby="newSpecificationTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newSpecificationTitle">{{ __('Ajouter Specification') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" method="POST" action="{{ route('admin.newSpecification') }}"
                data-action="Specifications#newSpecification">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="name">{{ __('Nom') }}</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="Tax">{{ __('tax') }}</label>
                            <input type="text" class="form-control" value="0" name="tax" id="tax" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="amount">{{ __('Montant') }}</label>
                            <input type="text" name="montant" value="0" class="form-control" id="amount" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<x-user-modify>
<div class="form-row">
    <div class="col-md-12 mb-3">
        <label for="d-name">{{ __('Nom') }}</label>
        <input type="text" class="form-control" name="name" id="d-name" required>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="d-Tax">{{ __('tax') }}</label>
        <input type="text" class="form-control" value="0" name="tax" id="d-tax" required>
    </div>
    <div class="col-md-6 mb-3">
        <label for="d-amount">{{ __('Montant') }}</label>
        <input type="text" name="amount" value="0" class="form-control" id="d-amount" required>
    </div>
</div>
</x-user-modify>

@endsection

