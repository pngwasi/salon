@extends('admin.specifications')

@section('specifications')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header">
            <button class="btn btn-secondary btn-sm btn-icon-split" data-toggle="modal"
                data-target="#newPayType">
                <span class="text">{{ __('Nouveau Mode') }}</span>
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Mode') }}</th>
                            <th scope="col">{{ __('Supprimer') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($pay_types as $type)
                        <tr>
                            <th scope="row">{{ $type->id }}</th>
                            <td>{{ $type->name }}</td>
                            <td><button
                                onclick="confirm('Supprimer ?') ? document.getElementById('delete-form-{{ $type->id }}').submit() : null" class="btn btn-danger btn-sm btn-icon-split">
                                <span class="text">{{ __('Supprimer') }}</span>
                            </button></td>
                            <form id="delete-form-{{ $type->id }}" action="{{ route('admin.deletePayType', ['id' => $type->id, 'redirect' => request()->fullUrl()]) }}" method="POST" style="display: none;">
                                @method('DELETE')
                                @csrf
                            </form>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="newPayType" tabindex="-1" role="dialog" aria-labelledby="newPayTypeTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newPayTypeTitle">{{ __('Ajouter le Mode Paie') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" method="POST" action="{{ route('admin.newPayType') }}"
                data-action="Specifications#newPayType">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="type_name">{{ __('Mode') }}</label>
                            <input type="text" class="form-control" name="type_name" id="type_name" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
