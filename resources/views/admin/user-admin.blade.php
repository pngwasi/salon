@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
<h1 class="h3 mb-0 text-gray-800">{{Auth::user('admin')->email }}</h1>
</div>
<div data-controller="UserAdmin">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.user.profile') ? 'active' : '' }}" href="{{ route('admin.user.profile') }}">{{ __('Profile') }}</a>
                </li>
                @if (Auth::user('admin')->can('view-super'))
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.user.settings') ? 'active' : '' }}" href="{{ route('admin.user.settings') }}">{{ __('Parametre') }}</a>
                </li>
                @endif
            </ul>
        </div>
        <div class="card-body">
            @yield('user-admin')
        </div>
    </div>
</div>
@endsection
