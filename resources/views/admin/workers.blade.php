@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Travailleurs') }}</h1>
</div>
<div data-controller="Worker">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.workers') ? 'active' : '' }}" href="{{ route('admin.workers') }}">{{ __('Travailleurs') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.cashier') ? 'active' : '' }}" href="{{ route('admin.cashier') }}">{{ __('Caissiers et Moderateurs') }}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('workers')
        </div>
    </div>
</div>

@endsection
