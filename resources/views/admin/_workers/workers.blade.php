@extends('admin.workers')

@section('workers')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-3">
                    <form action="" class=" d-inline-block form-inline w-100">
                        <div class="input-group">
                            <input type="text" class="form-control border-0 small" value="{{ request('search') }}"
                                name="search" placeholder="Recherche" />
                        </div>
                    </form>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal" data-target="#newWorker">
                        <span class="text">{{ __('Nouveau travailleur') }}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Email') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>
                            <th scope="col">{{ __('Adresse') }}</th>
                            <th scope="col">{{ __('Status') }}</th>
                            <th scope="col">{{ __('Profile') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                            <th scope="col">{{ __('Contact') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->phone }}</td>
                            <td>{{ $user->address }}</td>
                            <td class="{{ $user->moderator ? 'text-success': '' }}">
                                {{ $user->moderator ? 'Moderateur, ': '' }}
                                {{ $user->cashier ? 'Caissier': '' }}
                                {{ !$user->moderator && !$user->cashier ? 'Aucun': '' }}
                            </td>
                            <td class="{{ $user->active ? 'text-success': 'text-danger' }}">
                                @if($user->active)
                                <span class="badge badge-success">Actif</span>
                                @else
                                <span class="badge badge-danger">Inactif</span>
                                @endif
                            </td>
                            @include('template.admin.option-table',
                            ['user' => $user, 'routeModif' => 'admin.modifyWorker', 'routeDelete' => 'admin.deleteWorker'])
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $users->links() }}
</div>
<div class="modal fade" id="newWorker" tabindex="-1" role="dialog" aria-labelledby="newWorkerTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newWorkerTitle">{{ __('Ajouter Travailleur') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" method="POST" action="{{ route('admin.createWorker') }}"
                data-action="Worker#createWorker">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="name">{{ __('Nom') }}</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="email">{{ __('E-mail Adresse') }}</label>
                            <input type="email" class="form-control" name="email" id="email" required>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="address">{{ __('Adresse') }}</label>
                            <input type="text" name="address" class="form-control" id="address" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="phone">{{ __('Tel') }}</label>
                            <input type="tel" name="phone" class="form-control" id="phone" required>
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="password">{{ __('Mot de passe') }}</label>
                            <input type="password" name="password" class="form-control" id="password" required>
                        </div>

                        <div class="col-md-3 mb-3">
                            <label for="password_confirmation">{{ __('Confirmer mot de passe') }}</label>
                            <input type="password" name="password_confirmation" class="form-control"
                                id="password_confirmation" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="moderator" id="moderator">
                            <label class="form-check-label" for="moderator">
                                {{ __('Moderateur') }}
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="cashier" id="cashier">
                            <label class="form-check-label" for="cashier">
                                {{ __('Caissier') }}
                            </label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<x-user-modify>
<div class="form-row">
    <div class="col-md-4 mb-3">
        <label for="d-name">{{ __('Nom') }}</label>
        <input type="text" class="form-control" name="name" id="d-name" required>
    </div>
    <div class="col-md-4 mb-3">
        <label for="d-email">{{ __('E-mail Adresse') }}</label>
        <input type="email" class="form-control" name="email" id="d-email" required>
    </div>
    <div class="col-md-4 mb-3">
        <label for="d-address">{{ __('Adresse') }}</label>
        <input type="text" name="address" class="form-control" id="d-address" required>
    </div>
</div>
<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="d-phone">{{ __('Tel') }}</label>
        <input type="tel" name="phone" class="form-control" id="d-phone" required>
    </div>
    <div class="col-md-3 mb-3">
        <label for="d-password">{{ __('Mot de passe') }}</label>
        <input type="password" name="password" class="form-control" id="d-password">
    </div>

    <div class="col-md-3 mb-3">
        <label for="d-password_confirmation">{{ __('Confirmer mot de passe') }}</label>
        <input type="password" name="password_confirmation" class="form-control" id="d-password_confirmation">
    </div>
</div>
<div class="form-group">
    <div class="form-check">
        <input class="form-check-input" type="checkbox" name="moderator" id="d-moderator">
        <label class="form-check-label" for="d-moderator">
            {{ __('Moderateur') }}
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="checkbox" name="cashier" id="d-cashier">
        <label class="form-check-label" for="d-cashier">
            {{ __('Caissier') }}
        </label>
    </div>
</div>
</x-user-modify>


@endsection
