@extends('layouts.app-admin')

@section('admin')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">{{ __('Salaire') }}</h1>
</div>
<div data-controller="Admin-Salary">
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('admin.salary') ? 'active' : '' }}" 
                    href="{{ route('admin.salary') }}">{{ __('Salaire en cours') }}
                    <small>( <strong>Année: {{ now()->format('Y') }}, Mois: {{ now()->format('m') }}</strong> )</small></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Str::contains(Request::url(), route('admin.salary.salaries')) ? 'active' : '' }}"
                        href="{{ route('admin.salary.salaries') }}">{{ __('Salaires') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Str::contains(Request::url(), route('admin.salary.advance')) ? 'active' : '' }}"
                        href="{{ route('admin.salary.advance') }}">{{ __('Avance Salaire') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Str::contains( Request::url(), route('admin.salary.dept')) ? 'active' : '' }}"
                        href="{{ route('admin.salary.dept') }}">{{ __('Dette') }}</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('salary')
        </div>
    </div>
</div>

@endsection
