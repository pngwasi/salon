@extends('admin.subscribers')

@section('subscribers')
<div>
    <div class="col-md-6 mb-3">
        <div class="btn-group mt-4">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                aria-expanded="false">
                Recherche Abonné
            </button>
            <div class="dropdown-menu shadow">
                <div class="mx-1">
                    <div class="input-group">
                        <input type="text" data-url="{{ route('search.subscriber') }}" data-replace="true"
                            data-target="Subscribers.subscribersSeach" class="form-control bg-light border-0 small"
                            placeholder="Recherche...">
                    </div>
                </div>
                <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                <div data-target="Subscribers.subscribersList"></div>
            </div>
        </div>
    </div>
    <hr>
    <h4>{{ ucfirst($subscriber->name ?? '') }}</h4>
    <div class="content">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date d'entrée</th>
                        <th scope="col">Date d'Expiration</th>
                        <th scope="col">Montant</th>
                        <th scope="col">status</th>
                        <th scope="col">Modifier</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($subscriptions as $subscription)
                    <tr>
                        <th scope="row">{{ $subscription->id }}</th>
                        <td>{{ $subscription->start }}</td>
                        <td>{{ $subscription->end }}</td>
                        <td>{{ $subscription->amount }}</td>
                        <td>
                            @if($subscription->isValid())
                            <span class="badge badge-success">Actif</span>
                            @else
                            <span class="badge badge-danger">Inactif</span>
                            @endif
                        </td>
                        @include('template.admin.option-table',
                            ['user' => $subscription, 'contact' => true, 'routeModif' => 'admin.modifySubscription', 'routeDelete' => 'admin.deleteSubscription'])
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<x-user-modify>
<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="date_entry">{{ __('Date d\'entrée') }}</label>
        <input type="text" class="form-control" name="start" id="date_entry" required>
    </div>
    <div class="col-md-6 mb-3">
        <label for="date_expire">{{ __('Date d\'Expiration') }}</label>
        <input type="text" class="form-control" name="end" id="date_expire">
    </div>
</div>
<div class="form-row">
    <div class="col-md-7 mb-3">
        <label for="amount">{{ __('Montant') }}</label>
        <input type="text" class="form-control" value="0" name="amount" id="montant" required>
    </div>
    <div class="col-md-5 mb-3">
        <label for="per">{{ __('Par Coiffure') }}</label>
        <input type="text" class="form-control" value="0" name="per" id="per" required>
    </div>
</div>
</x-user-modify>

@endsection
