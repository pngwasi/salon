@extends('admin.subscribers')

@section('subscribers')
<div class="row">
    <div class="col-lg-6 col-md-12 mb-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <div>Profile Abonné</div>
            </div>
            <div class="card-body">
                <div id="show-onsub">
                    <div class="form-row">
                        <div class="col-lg-8 mb-3">
                            <div>
                                <button type="button" class="btn text-white btn-block btn-info dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Abonné
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.subscriber') }}"
                                                data-target="Subscribers.scb"
                                                class="form-control bg-light border-0 small"
                                                placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="Subscribers.scbList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-3">
                            <input type="text" class="form-control" id="subscriber_name" readonly
                                placeholder="Abonné">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-lg-6 mb-3">
        <div class="card">
            <div class="card-header">Abonné Profile <small>( <strong>Année: {{ now()->format('Y') }}, Mois:
                        {{ now()->format('m') }}</strong> )</small></div>
            <div class="card-body">
                <div data-target="Subscribers.scbCard" data-url="{{ route('collect.profile.subscriber') }}">
                    <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Nom</h6>
                                <small class="text-muted">
                                    <span data-name></span>
                                </small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Email</h6>
                                <small class="text-muted">
                                    <span data-email></span>
                                </small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <div>
                                <h6 class="my-0">Phone</h6>
                                <small class="text-muted">
                                    <span data-phone></span>
                                </small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Abonnement</span>
                            <h5><span class="badge badge-secondary badge-pill"><strong>
                                        <span data-amount-scp></span>
                                    </strong></span></h5>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Total Coiffure</span>
                            <h5><span class="badge badge-info badge-pill text-white"><strong>
                                        <span data-total-hm></span>
                                    </strong></span></h5>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Par Coiffure</span>
                            <h5><span class="badge badge-info badge-pill text-white"><strong>
                                        $<span data-per-h></span>
                                    </strong></span></h5>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Reste</span>
                            <h5><span class="badge badge- badge-success text-white"><strong>
                                        <span data-rest-h></span>
                                    </strong></span></h5>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection