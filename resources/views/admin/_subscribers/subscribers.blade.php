@extends('admin.subscribers')

@section('subscribers')
<div class="justify-content-center">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-3">
                    <form action="" class=" d-inline-block form-inline w-100">
                        <div class="input-group">
                            <input type="text" class="form-control border-0 small" value="{{ request('search') }}" name="search" placeholder="Recherche" />
                        </div>
                    </form>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal"
                        data-target="#newSubscriber">
                        <span class="text">{{ __('Nouveau Abonné') }}</span>
                    </button>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-primary btn-sm btn-icon-split" data-toggle="modal"
                        data-target="#newSubscription">
                        <span class="text">{{ __('Nouveau Abonnement') }}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{{ __('Nom') }}</th>
                            <th scope="col">{{ __('Email') }}</th>
                            <th scope="col">{{ __('Tel') }}</th>
                            <th scope="col">{{ __('Adresse') }}</th>
                            <th scope="col">{{ __('Profile') }}</th>
                            <th scope="col">{{ __('Modifier') }}</th>
                            <th scope="col">{{ __('Contact') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subscribers as $subscriber)
                        <tr>
                            <th scope="row">{{ $subscriber->id }}</th>
                            <td>{{ $subscriber->name }}</td>
                            <td>{{ $subscriber->email }}</td>
                            <td>{{ $subscriber->phone }}</td>
                            <td>{{ $subscriber->address }}</td>
                            <td>
                                @if($subscriber->active)
                                <span class="badge badge-success">Actif</span>
                                @else
                                <span class="badge badge-danger">Inactif</span>
                                @endif
                            </td>
                            @include('template.admin.option-table',
                                ['user' => $subscriber, 'routeModif' => 'admin.modifySubsriber', 'routeDelete' => 'admin.deleteSubsriber'])
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{ $subscribers->links() }}
</div>
<div class="modal fade" id="newSubscriber" tabindex="-1" role="dialog" aria-labelledby="newSubscriberTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newSubscriberTitle">{{ __('Ajouter Abonné') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.newSubscriber') }}"
                data-action="Subscribers#newSubscriber">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="name">{{ __('Nom') }}</label>
                            <input type="text" class="form-control" name="name" id="name" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email">{{ __('E-mail') }} (Optionel)</label>
                            <input type="email" class="form-control" name="email" id="email">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="phone">{{ __('Tel') }}</label>
                            <input type="tel" name="phone" class="form-control" id="phone" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="Address">{{ __('Adresse') }}</label>
                            <input type="text" name="address" class="form-control" id="Address" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<x-user-modify>
<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="d-name">{{ __('Nom') }}</label>
        <input type="text" class="form-control" name="name" id="d-name" required>
    </div>
    <div class="col-md-6 mb-3">
        <label for="d-email">{{ __('E-mail') }} (Optionel)</label>
        <input type="email" class="form-control" name="email" id="d-email">
    </div>
</div>
<div class="form-row">
    <div class="col-md-6 mb-3">
        <label for="d-phone">{{ __('Tel') }}</label>
        <input type="tel" name="phone" class="form-control" id="d-phone" required>
    </div>
    <div class="col-md-6 mb-3">
        <label for="d-Address">{{ __('Adresse') }}</label>
        <input type="text" name="address" class="form-control" id="d-Address" required>
    </div>
</div>
</x-user-modify>

<div class="modal fade" id="newSubscription" tabindex="-1" role="dialog" aria-labelledby="newSubscriptionTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newSubscriptionTitle">{{ __('Ajouter Abonnement') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="needs-validation" autocomplete="off" method="POST" action="{{ route('admin.newSubscription') }}"
                data-action="Subscribers#newSubscription">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <div class="btn-group mt-4">
                                <button type="button" class="btn btn-secondary dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Abonné
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.subscriber') }}"
                                                data-target="Subscribers.subscribersSeach"
                                                class="form-control bg-light border-0 small"
                                                placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="Subscribers.subscribersList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email_phone">{{ __('Abonné (Email ou Tél)') }}</label>
                            <input type="text" class="form-control" data-target="Subscribers.subscriberField"
                                name="email_phone" id="email_phone" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="date_entry">{{ __('Date d\'entrée') }}</label>
                            <input type="text" class="form-control" name="date_entry" id="date_entry" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="date_expire">{{ __('Date d\'Expiration') }}</label>
                            <input type="text" class="form-control" name="date_expire" id="date_expire">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-7 mb-3">
                            <label for="amount">{{ __('Montant') }}</label>
                            <input type="text" class="form-control" value="0" name="montant" id="montant" required>
                        </div>
                        <div class="col-md-5 mb-3">
                            <label for="per">{{ __('Par Coiffure') }}</label>
                            <input type="text" class="form-control" value="0" name="per" id="per" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Fermer') }}</button>
                    <button type="submit" class="btn btn-primary">{{ __('Enregistrer') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>  
@endsection

