@extends('layouts.home-app')

@section('home-content')
<h4>Contact</h4>
<div class="tchat-contact mt-3">
    <div class="chat-container">
        <div class="row h-100 sidebar--p justify-content-center">
            <div class="col-12 col-md-9 d-flex p-0 card-message">
                <div class="card">
                    <div class="card-body bg-lightgrey d-flex flex-column p-0" style="flex: 9 1">
                        <div style="min-height: 8rem;" class="container-fluid message-scroll" style="flex: 1 1">
                            <div class="row">
                                <div class="card message-card m-1">
                                    <div class="card-body p-2">
                                        <span class="mx-2">Hi, Dave</span>
                                        <span class="float-right mx-1"><small>14:13</small></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="card message-card bg-lightblue m-1">
                                    <div class="card-body p-2">
                                        <span class="mx-2">Hello, Stan</span>
                                        <span class="float-right mx-1"><small>14:14</small></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="card message-card bg-lightblue m-1">
                                    <div class="card-body p-2">
                                        <span class="mx-2">What's up?</span>
                                        <span class="float-right mx-1"><small>14:14</small></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="card message-card m-1">
                                    <div class="card-body p-2">
                                        <span>So far so good, but my plumbus doesn't work as well as
                                            Meeseeks can't fix it, please, help me or they... They gonna
                                            kill Morty...</span>
                                        <span class="float-right"><small>14:16</small></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="card message-card bg-lightblue m-1">
                                    <div class="card-body p-2">
                                        <span>I've called Rick, I'm on the way to your house,
                                            but probably I lost my portal gun at the party yesterday.
                                            Anyway, don't call the other Meeseeks solve this shit.</span>
                                        <span class="float-right mx-1"><small>14:21</small></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="card message-card bg-lightblue m-1">
                                    <div class="card-body p-2">
                                        <span>I've called Rick, I'm on the way to your house,
                                            but probably I lost my portal gun at the party yesterday.
                                            Anyway, don't call the other Meeseeks solve this shit.</span>
                                        <span class="float-right mx-1"><small>14:21</small></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-4 justify-content-center">
            <div class="col-12 col-md-9">
                <div class="input-group">
                    <input type="text" class="form-control border-0 bg-light" placeholder="Ecrire...">
                    <span class="input-group-addon ml-5">
                        <button type="button" class="btn btn-primary btn-sm">Envoyer message</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
