<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="{{ mix('assets/vendor/css/app.css') }}" rel="stylesheet">
    @yield('style')
</head>
<body>
    <div id="s--app">
        @yield('content')
        <x-report-modal />
    </div>
    <div class="loading-ntf hidden">
        <div class='uil-ring-css' style='transform:scale(0.79);'>
            <spinning-dots style="width:100px;stroke-width:20px;color: #68c3ff;"></spinning-dots>
        </div>
    </div>
    <script src="{{ mix('assets/vendor/js/app.js') }}"></script>
    @yield('script')
</body>
</html>
