@extends('layouts.app')

@section('content')
@include('layouts.navbar')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card mb-5">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{ Str::contains(Request::url(), route('home')) ? 'active' : '' }}"  href="{{ route('home') }}">{{ __('Acceuil') }}</a>
                        </li>
                        @if (Auth::user('web')->can('not-cashier'))
                        <li class="nav-item">
                            <a class="nav-link {{ Str::contains(Request::url(), route('hairdress')) ? 'active' : '' }}"  href="{{ route('hairdress') }}">{{ __('Coiffure') }}</a>
                        </li>
                        @endif
                        @if (Auth::user('web')->can('view-cashier'))
                            <li class="nav-item">
                                <a class="nav-link {{ Str::contains(Request::url(), route('collect')) ? 'active' : '' }}" href="{{ route('collect') }}" >{{ __('Encaissement') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Str::contains(Request::url(), route('disburse')) ? 'active' : '' }}" href="{{ route('disburse') }}" >{{ __('Descaissement') }}</a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link {{ Str::contains(Request::url(), route('report')) ? 'active' : '' }}" href="{{ route('report') }}" >{{ __('Contacter l\'administrateur') }}</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    @yield('home-content')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
