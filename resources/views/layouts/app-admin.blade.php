@extends('layouts.app')

@section('style')
<link rel="stylesheet" href="{{ asset('assets/sb-admin-2/css/sb-admin-2.min.css') }}">
@endsection

@section('content')
<div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('admin.home') }}">
            <img src="{{ asset('assets/images/logo-nepa-miroire-1m.jpg') }}" width="30" height="30" class="d-inline-block align-top" alt="">
            <div class="sidebar-brand-text mx-3">{{ config('app.name', 'Laravel') }}</div>
        </a>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Request::url() == route('admin.home') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.home') }}">
                <span>Tableau de bord</span></a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.workers')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.workers') }}">
                <span>{{ __('Travailleurs') }}</span>
            </a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.specifications')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.specifications') }}">
                <span>{{ __('Specifications') }}</span>
            </a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.subscribers')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.subscribers') }}">
                <span>{{ __('Abonnés') }}</span>
            </a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.products.inventory')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.products.inventory') }}">
                <span>{{ __('Materiels') }}</span>
            </a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.collection.hairdressing')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.collection.hairdressing') }}"><span>{{ __('Entrés') }}</span></a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.disbursement')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.disbursement') }}"><span>{{ __('Sorties') }}</span></a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.salary')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.salary') }}">
                <span>{{ __('Salaire') }}</span></a>
        </li>
        <hr class="sidebar-divider my-0">
        <li class="nav-item {{ Str::contains(Request::url(), route('admin.report')) ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('admin.report') }}">
                <span>{{ __('Rapport') }}</span></a>
        </li>
        <hr class="sidebar-divider d-none d-md-block">
    </ul>
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small"
                            placeholder="{{ __('Recherche') }}" aria-label="Search" aria-describedby="basic-addon2">
                    </div>
                </form>
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item dropdown no-arrow d-sm-none">
                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                            aria-labelledby="searchDropdown">
                            <form class="form-inline mr-auto w-100 navbar-search">
                                <div class="input-group">
                                    <input type="text" class="form-control bg-light border-0 small"
                                        placeholder="{{ __('Recherche') }}" aria-label="Search"
                                        aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="button">
                                            <i class="fas fa-search fa-sm"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>


                    <div class="topbar-divider d-none d-sm-block"></div>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user('admin')->email }}</span>
                            <img src="{{ asset('assets/images/avatar.jpg') }}" width="30" height="30" style="border-radius: 25%" class="d-inline-block align-top" alt="">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                            aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="{{ route('admin.user.profile') }}">
                                Profile
                            </a>
                            @if (Auth::user('admin')->can('view-super'))
                            <a class="dropdown-item" href="{{ route('admin.user.settings') }}">
                                Parametre
                            </a>
                            @endif
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('admin.logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Deconnexion') }}
                                    </a>
                            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="container-fluid">
                @yield('admin')
            </div>
            @include('template.contact')
        </div>
    </div>
</div>
@endsection
