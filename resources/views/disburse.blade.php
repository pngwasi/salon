@extends('layouts.home-app')

@section('home-content')
<div data-controller="User-Disburse">
    <h4>Decaissement</h4>
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs justify-content-center" role="tablist">
                <li class="nav-item">
                    <a class="nav-link {{ Request::url() == route('disburse') ? 'active' : '' }}"  href="{{ route('disburse') }}">{{ __('Acceuil') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Str::contains(Request::url(), route('disburse.sortie')) ? 'active' : '' }}"  href="{{ route('disburse.sortie') }}">{{ __('Sorties') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Str::contains(Request::url(), route('disburse.unconfirmed')) ? 'active' : '' }}"  href="{{ route('disburse.unconfirmed') }}">{{ __('En attente') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Str::contains(Request::url(), route('disburse.rejected')) ? 'active' : '' }}"  href="{{ route('disburse.rejected') }}">{{ __('Sorties Rejetées') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Str::contains(Request::url(), route('disburse.search')) ? 'active' : '' }}"  href="{{ route('disburse.search') }}">{{ __('Recherche') }}</a>
                </li>
            </ul>
            <div class="content mt-3">
                @yield('disburse')
            </div>
        </div>
    </div>
</div>
@endsection
