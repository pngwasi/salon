@extends('disburse')

@section('disburse')
<div class="row justify-content-center">
    <div class="col-md-8 col-sm-12">
        <div class="card mt-4">
            <div class="card-header">
                <h5><span class="badge badge-info text-white">Nouvelle Sortie</span></h5>
            </div>
            <div class="card-body">
                <form class="needs-validation" data-action="User-Disburse#newSortie" action="{{ route('disburse.newSortie') }}" method="POST" autocomplete="off" novalidate>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="montant">Montant</label>
                            <input type="number" class="form-control" name="montant" id="montant" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="recipient">Bénéficiaire</label>
                            <input type="text" class="form-control" name="recipient" id="recipient" required>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="motif">Motif</label>
                            <textarea name="motif" id="motif" required class="form-control" rows="1"></textarea>
                        </div>
                    </div>
                    <div class="mt-3">
                        <button class="btn btn-primary" type="submit">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection