@extends('disburse')

@section('disburse')
<div id="chart-home">
    <div class="row justify-content-center mb-3">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <canvas id="disburse_chart" style="display: block; width: 100%; height: 300px;"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-10 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <span>Sorties</span>
                </div>
                <div class="card-body">
                    @include('_disburse._disburse-table', ['disburse' => $disburse])
                    <div class="justify-content-center">
                        {{ $disburse->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.chart_disburse = @json($chart_disburse);
</script>
@endsection
