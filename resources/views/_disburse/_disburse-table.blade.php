<table class="table m-0 text-muted">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Montant</th>
            <th scope="col">Bénéficiaire</th>
            <th scope="col">Motif</th>
            <th scope="col">Date</th>
            <th scope="col">Status</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($disburse as $bn)
        <tr>
            <td>{{ $bn->id }}</td>
            <td><span class="badge badge-info text-white">${{ $bn->amount }}</span></td>
            <td>{{ $bn->recipient }}</td>
            <td><small>{{ $bn->motif }}</small></td>
            <td>{{ $bn->created_at }}</td>
            <td>
                @if ($bn->approved)
                <span class="badge badge-success">Apprové</span>
                @endif
                @if ($bn->rejected)
                <span class="badge badge-danger">rejeté</span>
                @endif
                @if (!$bn->rejected && !$bn->approved)
                    <span class="badge badge-warning">En attente de confirmation</span>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>