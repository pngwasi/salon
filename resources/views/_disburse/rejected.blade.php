@extends('disburse')

@section('disburse')
<div class="row justify-content-center">
    <div class="col-md-10 col-sm-12">
        <div class="card mt-4">
            <div class="card-header">
                <h5><span class="badge badge-warning">Sorties rejetées</span></h5>
            </div>
            <div class="card-body">
                @include('_disburse._disburse-table', ['disburse' => $disburse])
                <div class="justify-content-center">
                    {{ $disburse->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection