@extends('layouts.home-app')

@section('home-content')
<br />
<div class="row justify-content-center">
    <div class="col-md-7">
        <div class="card mb-3 shadow-sm border-light">
            <div class="card-header"><strong>{{ Auth::user('web')->email }}</strong></div>
            <div class="card-body text-center">
                <table class="table table-borderless text-muted">
                    <thead>
                        <tr>
                            <th scope="col">E-mail</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Adresse</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ Auth::user('web')->email }}</td>
                            <td>{{ Auth::user('web')->name }}</td>
                            <td>{{ Auth::user('web')->address }}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table table-borderless text-muted">
                    <thead>
                        <tr>
                            <th scope="col">Phone</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ Auth::user('web')->phone }}</td>
                            <td>
                                @if (Auth::user('web')->moderator)
                                <span class="badge badge-primary">Moderateur</span>
                                @endif
                                @if (Auth::user('web')->cashier)
                                <span class="badge badge-primary">Caissier</span>
                                @endif
                                @if (!Auth::user('web')->moderator && !Auth::user('web')->cashier)
                                <span class="badge badge-secondary">Coiffeur</span>
                                @endif
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="card mb-3 shadow-sm border-light">
            <div class="card-header"><strong>Changer le mot de passe</strong></div>
            <div class="card-body">
                @if (session('saved'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('saved') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                @endif
                <form action="{{ route('change-password') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-row">
                        <div class="col-12 mb-3">
                            <label for="password">Mot de passe</label>
                            <input type="password" name="motDePasse" class="form-control @error('motDePasse') is-invalid @enderror" id="password" required>
                            @error('motDePasse')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-12 mb-3">
                            <label for="new_password">Nouveau Mot de Passe</label>
                            <input type="password" name="nouveau_motDePasse" class="form-control @error('nouveau_motDePasse') is-invalid @enderror" id="new_password" required>
                            @error('nouveau_motDePasse')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-12 mb-3">
                            <label for="new_password_confirmation">Confirmer Mot de Passe</label>
                            <input type="password" name="nouveau_motDePasse_confirmation" class="form-control @error('nouveau_motDePasse_confirmation') is-invalid @enderror"
                                id="new_password_confirmation" required>
                            @error('nouveau_motDePasse_confirmation')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <input type="hidden" name="redirect" value="{{ route('home') }}">
                    </div>
                    <button class="btn btn-primary" type="submit">Enregistrer</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
