@extends('_collect.bonus')

@section('bonus')
<div class="row justify-content-center">
    <div class="col-md-12 mb-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <form method="GET" class="d-inline-block" action="" autocomplete="off">
                    <input type="hidden" name="uid" value="{{ request('uid') }}">
                    <div class="form-row">
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <div>
                                <button type="button" class="btn  btn-block btn-outline-info dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Coiffeur
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.worker') }}"
                                                data-target="User-Collect.hlsS"
                                                class="form-control bg-light border-0 small" placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="User-Collect.hlsSList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <input type="text" class="form-control" id="worker_nameS" value="{{ $us ? $us->name : '' }}" readonly
                                placeholder="Coiffeur (optionnel)">
                            <div class="valid-feedback"></div>
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <input type="number" class="form-control" name="day" value="{{ request('day') }}"
                                placeholder="Jour (optionnel)">
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <input type="number" class="form-control" name="month" value="{{ request('month') }}"
                                placeholder="Mois (optionnel)">
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <input type="number" class="form-control" value="{{ request('year') }}" name="year"
                                placeholder="Year">
                        </div>
                        <div class="col-lg-2 col-sm-12 col-sm-12 mb-3">
                            <button type="submit" class="btn btn-primary">recherche</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-body">
                <table class="table m-0 text-muted">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Coiffeur</th>
                            <th scope="col">Montant</th>
                            <th scope="col">Date</th>
                            <th scope="col">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($bonuses as $bn)
                        <tr>
                            <td>{{ $bn->id }}</td>
                            <td>{{ $bn->user->name }}</td>
                            <td><span class="badge badge-info text-white">${{ $bn->amount }}</span></td>
                            <td>{{ $bn->created_at }}</td>
                            <td>
                                @if ($bn->approved)
                                <span class="badge badge-success">Apprové</span>
                                @endif
                                @if ($bn->rejected)
                                <span class="badge badge-danger">rejeté</span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="justify-content-center">
                    {{ $bonuses->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
