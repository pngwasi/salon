@extends('_collect.bonus')

@section('bonus')
<div class="row">
    <div class="col-lg-6 col-md-12 mb-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <div>Ajouter Bonus</div>
            </div>
            <div class="card-body">
                <form class="needs-validation" data-action="User-Collect#newBonus" action="{{ route('collect.bonus.newBonus') }}" method="POST" autocomplete="off" novalidate>
                    <input type="hidden" name="coiffeur">
                    <input type="hidden" name="coiffeur_email">
                    <div class="form-row">
                        <div class="col-lg-8 mb-3">
                            <div>
                                <button type="button" class="btn  btn-block btn-secondary dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Coiffeur
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.worker') }}"
                                                data-target="User-Collect.hls"
                                                class="form-control bg-light border-0 small" placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="User-Collect.hlsList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-3">
                            <input type="text" class="form-control" id="worker_name" readonly placeholder="Coiffeur">
                            <div class="valid-feedback"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="montant">Montant</label>
                            <input type="number" class="form-control" name="montant" id="montant" required>
                        </div>
                    </div>
                    <div class="mt-3">
                        <button class="btn btn-primary" type="submit">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-header">Coiffeur Profile <small>( <strong>Date:
                        {{ now()->format('Y-m-d') }}</strong> )</small></div>
            <div class="card-body">
                <div data-target="User-Collect.hlsCard" data-url="{{ route('collect.profile.worker') }}">
                    <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Nom</h6>
                                <small class="text-muted">
                                    <span data-name></span>
                                </small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Email</h6>
                                <small class="text-muted">
                                    <span data-email></span>
                                </small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <div>
                                <h6 class="my-0">Phone</h6>
                                <small class="text-muted">
                                    <span data-phone></span>
                                </small>
                            </div>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Montant Coiffure</span>
                            <h5><span class="badge badge-secondary badge-pill"><strong>
                                        $<span data-amount-h></span>
                                    </strong></span></h5>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Nombre Coiffure</span>
                            <h5><span class="badge badge-info badge-pill text-white"><strong>
                                        <span data-total-h></span>
                                    </strong></span></h5>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Bonus</span>
                            <h5><span class="badge badge- badge-success text-white"><strong>
                                        $<span data-bonus></span>
                                    </strong></span></h5>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection