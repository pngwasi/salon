@extends('collect')

@section('collect')
<div id="chart-home">
    <div class="row justify-content-center mb-3">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <canvas id="collect_chart" style="display: block; width: 100%; height: 300px;"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-10 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <span>Coiffures</span>
                </div>
                <div class="card-body">
                    <table class="table m-0 text-muted">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Coiffeur</th>
                                <th scope="col">Montant</th>
                                <th scope="col">Abonné</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                                <th scope="col">Spécification</th>
                                <th scope="col">Mode Paie</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($collect as $bn)
                            <tr>
                                <td>{{ $bn->id }}</td>
                                <td>{{ $bn->user->name }}</td>
                                <td><span class="badge badge-info text-white">${{ $bn->amount }}</span></td>
                                <td>
                                    @if ($bn->subscriber)
                                    {{ $bn->subscriber->name }}
                                    @else
                                    --
                                    @endif
                                </td>
                                <td>{{ $bn->created_at }}</td>
                                <td>
                                    @if ($bn->approved)
                                    <span class="badge badge-success">Apprové</span>
                                    @endif
                                    @if ($bn->rejected)
                                    <span class="badge badge-danger">rejeté</span>
                                    @endif
                                </td>
                                <td> 
                                    @if ($bn->specification)
                                        <span class="badge badge-secondary">
                                            {{ Str::ucfirst($bn->specification->name)  }} {{ Str::ucfirst($bn->specification->amount)  }}
                                        </span>
                                    @endif
                                </td>
                                <td>{{ $bn->modePay ? $bn->modePay->name: '' }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="justify-content-center">
                        {{ $collect->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.chart_collect = @json($chart_collect);
</script>
@endsection
