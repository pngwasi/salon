@extends('collect')

@section('collect')
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('collect.entries') ? 'active' : '' }}"  href="{{ route('collect.entries') }}">{{ __('Nouvelle Entré') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('collect.entries.search') ? 'active' : '' }}"  href="{{ route('collect.entries.search') }}">{{ __('Recherche') }}</a>
            </li>
        </ul>
        <div class="content mt-4">
            @yield('entries')
        </div>
    </div>
</div>
@endsection
