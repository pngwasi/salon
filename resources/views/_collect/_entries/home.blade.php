@extends('_collect.entries')

@section('entries')
<div class="row">
    <div class="col-lg-7 col-md-12 mb-3">
        <div class="card">
            <div class="card-header d-flex justify-content-between">
                <div>Entrer</div>
                <button type="button" class="btn btn-sm btn-info text-white"
                    data-action="User-Collect#reset">actualiser</button>
            </div>
            <div class="card-body">
                <form class="needs-validation" data-action="User-Collect#newEntry"
                    action="{{ route('collect.profile.newEntry') }}" autocomplete="off" novalidate>
                    <input type="hidden" name="coiffeur">
                    <input type="hidden" name="coiffeur_email">
                    <input type="hidden" name="abonne">
                    <input type="hidden" name="abonne_email">
                    <div class="form-row">
                        <div class="col-lg-8 mb-3">
                            <div>
                                <button type="button" class="btn  btn-block btn-secondary dropdown-toggle"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Recherche Coiffeur
                                </button>
                                <div class="dropdown-menu shadow">
                                    <div class="mx-1">
                                        <div class="input-group">
                                            <input type="text" data-url="{{ route('search.worker') }}"
                                                data-target="User-Collect.hls"
                                                class="form-control bg-light border-0 small" placeholder="Recherche...">
                                        </div>
                                    </div>
                                    <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                    <div data-target="User-Collect.hlsList"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 mb-3">
                            <input type="text" class="form-control" id="worker_name" readonly placeholder="Coiffeur">
                            <div class="valid-feedback"></div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-4 mb-3">
                            <label for="specification">Spécification</label>
                            <select class="custom-select" data-action="User-Collect#specificationValue" name="specification" id="specification"
                                required>
                                <option selected disabled value="">Choisir...</option>
                                @foreach ($specifications as $specification)
                                <option value="{{ $specification->id }}" data-amount="{{ $specification->amount }}">
                                    {{ $specification->name }} {{ $specification->amount }}$</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="mode_pay">Mode Paie</label>
                            <select class="custom-select" name="mode_pay" id="mode_pay" required>
                                <option selected disabled value="">Choisir...</option>
                                @foreach ($payTypes as $payType)
                                <option value="{{ $payType->id }}">{{ $payType->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="montant">Montant</label>
                            <input type="number" class="form-control" name="montant" id="montant" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="has_subscriber"
                                onchange="event.target.checked ? $('#show-onsub').fadeIn() : $('#show-onsub').fadeOut()"
                                class="custom-control-input" id="onSubscriber">
                            <label class="custom-control-label" for="onSubscriber">Abonné ?</label>
                        </div>
                    </div>
                    <div id="show-onsub" style="display: none;">
                        <div class="form-row">
                            <div class="col-lg-8 mb-3">
                                <div>
                                    <button type="button" class="btn text-white btn-block btn-info dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Recherche Abonné
                                    </button>
                                    <div class="dropdown-menu shadow">
                                        <div class="mx-1">
                                            <div class="input-group">
                                                <input type="text" data-url="{{ route('search.subscriber') }}"
                                                    data-target="User-Collect.scb"
                                                    class="form-control bg-light border-0 small"
                                                    placeholder="Recherche...">
                                            </div>
                                        </div>
                                        <div class="dropdown-divider mx-2" style="padding: 0 150px"></div>
                                        <div data-target="User-Collect.scbList"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 mb-3">
                                <input type="text" class="form-control" id="subscriber_name" readonly
                                    placeholder="Abonné">
                            </div>
                        </div>
                    </div>
                    <div class="mt-3">
                        <button class="btn btn-primary" type="submit">Enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-5 col-md-12">
        <div class="row">
            <div class="col-lg-12 col-md-6 col-sm-12 mb-3">
                <div class="card">
                    <div class="card-header">Coiffeur Profile <small>( <strong>Date:
                                {{ now()->format('Y-m-d') }}</strong> )</small></div>
                    <div class="card-body">
                        <div data-target="User-Collect.hlsCard" data-url="{{ route('collect.profile.worker') }}">
                            <ul class="list-group mb-3">
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Nom</h6>
                                        <small class="text-muted">
                                            <span data-name></span>
                                        </small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Email</h6>
                                        <small class="text-muted">
                                            <span data-email></span>
                                        </small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <div>
                                        <h6 class="my-0">Phone</h6>
                                        <small class="text-muted">
                                            <span data-phone></span>
                                        </small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Montant Coiffure</span>
                                    <h5><span class="badge badge-secondary badge-pill"><strong>
                                                $<span data-amount-h></span>
                                            </strong></span></h5>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Nombre Coiffure</span>
                                    <h5><span class="badge badge-info badge-pill text-white"><strong>
                                                <span data-total-h></span>
                                            </strong></span></h5>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Bonus</span>
                                    <h5><span class="badge badge- badge-success text-white"><strong>
                                                $<span data-bonus></span>
                                            </strong></span></h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-6 col-sm-12 mb-3">
                <div class="card">
                    <div class="card-header">Abonné Profile <small>( <strong>Année: {{ now()->format('Y') }}, Mois:
                                {{ now()->format('m') }}</strong> )</small></div>
                    <div class="card-body">
                        <div data-target="User-Collect.scbCard" data-url="{{ route('collect.profile.subscriber') }}">
                            <ul class="list-group mb-3">
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Nom</h6>
                                        <small class="text-muted">
                                            <span data-name></span>
                                        </small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between lh-condensed">
                                    <div>
                                        <h6 class="my-0">Email</h6>
                                        <small class="text-muted">
                                            <span data-email></span>
                                        </small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <div>
                                        <h6 class="my-0">Phone</h6>
                                        <small class="text-muted">
                                            <span data-phone></span>
                                        </small>
                                    </div>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Abonnement</span>
                                    <h5><span class="badge badge-secondary badge-pill"><strong>
                                                <span data-amount-scp></span>
                                            </strong></span></h5>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Total Coiffure</span>
                                    <h5><span class="badge badge-info badge-pill text-white"><strong>
                                                <span data-total-hm></span>
                                            </strong></span></h5>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Par Coiffure</span>
                                    <h5><span class="badge badge-info badge-pill text-white"><strong>
                                                $<span data-per-h></span>
                                            </strong></span></h5>
                                </li>
                                <li class="list-group-item d-flex justify-content-between">
                                    <span>Reste</span>
                                    <h5><span class="badge badge- badge-success text-white"><strong>
                                                <span data-rest-h></span>
                                            </strong></span></h5>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
