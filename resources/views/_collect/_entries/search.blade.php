@extends('_collect.entries')

@section('entries')
<div class="card">
    <div class="card-header">
        <form method="GET" action="" autocomplete="off">
            <div class="form-row">
                <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                    <input type="number" class="form-control" name="day" value="{{ request('day') }}"
                        placeholder="Jour (optionnel)">
                </div>
                <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                    <input type="number" class="form-control" name="month" value="{{ request('month') }}"
                        placeholder="Mois (optionnel)">
                </div>
                <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                    <input type="number" class="form-control" value="{{ request('year') }}" name="year"
                        placeholder="Year">
                </div>
                <div class="col-lg-3 col-sm-12 col-sm-12 mb-3">
                    <button type="submit" class="btn btn-primary">recherche</button>
                </div>
            </div>
        </form>
    </div>
    <div class="card-body">
        <table class="table m-0 text-muted">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Coiffeur</th>
                    <th scope="col">Montant</th>
                    <th scope="col">Abonné</th>
                    <th scope="col">Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">Spécification</th>
                    <th scope="col">Mode Paie</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($collect as $bn)
                <tr>
                    <td>{{ $bn->id }}</td>
                    <td>{{ $bn->user->name }}</td>
                    <td><span class="badge badge-info text-white">${{ $bn->amount }}</span></td>
                    <td>
                        @if ($bn->subscriber)
                        {{ $bn->subscriber->name }}
                        @else
                        --
                        @endif
                    </td>
                    <td>{{ $bn->created_at }}</td>
                    <td>
                        @if ($bn->approved)
                        <span class="badge badge-success">Apprové</span>
                        @endif
                        @if ($bn->rejected)
                        <span class="badge badge-danger">rejeté</span>
                        @endif
                    </td>
                    <td> 
                        @if ($bn->specification)
                            <span class="badge badge-secondary">
                                {{ Str::ucfirst($bn->specification->name)  }} {{ Str::ucfirst($bn->specification->amount)  }}
                            </span>
                        @endif
                    <td>{{ $bn->modePay ? $bn->modePay->name: '' }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="justify-content-center">
            {{ $collect->links() }}
        </div>
    </div>
</div>
@endsection