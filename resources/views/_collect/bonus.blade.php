@extends('collect')

@section('collect')
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('collect.bonus') ? 'active' : '' }}"  href="{{ route('collect.bonus') }}">{{ __('Recherche') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Request::url() == route('collect.bonus.add-bonus') ? 'active' : '' }}"  href="{{ route('collect.bonus.add-bonus') }}">{{ __('Ajouter bonus') }}</a>
            </li>
        </ul>
        <div class="content mt-4">
            @yield('bonus')
        </div>
    </div>
</div>
@endsection
