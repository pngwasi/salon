@extends('hairdress')

@section('hairdress')
<div class="card">
    <div class="card-header">
        <form method="GET" action="" autocomplete="off">
            <div class="form-row">
                <div class="col-lg-4 col-sm-12 col-sm-12 mb-3">
                    <input type="number" class="form-control" name="month" value="{{ request('month') }}" placeholder="Mois (optionnel)">
                </div>
                <div class="col-lg-4 col-sm-12 col-sm-12 mb-3">
                    <input type="number" class="form-control" value="{{ request('year') }}" name="year" placeholder="Year">
                </div>
                <div class="col-lg-4 col-sm-12 col-sm-12 mb-3">
                    <button type="submit" class="btn btn-primary">recherche</button>
                </div>
            </div>
        </form>
    </div>
    <div class="card-body">
        <table class="table m-0 text-muted">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Montant</th>
                    <th scope="col">Mois</th>
                    <th scope="col">Année</th>
                    <th scope="col">Date</th>
                    <th scope="col">Status</th>
                    <th scope="col">remboursé</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($depts as $bn)
                <tr>
                    <td>{{ $bn->id }}</td>
                    <td><span class="badge badge-info text-white">${{ $bn->amount }}</span></td>
                    <td>{{ $bn->month }}</td>
                    <td>{{ $bn->year }}</td>
                    <td>{{ $bn->created_at }}</td>
                    <td>
                        @if ($bn->approved)
                        <span class="badge badge-success">Apprové</span>
                        @endif
                        @if ($bn->rejected)
                        <span class="badge badge-danger">rejeté</span>
                        @endif
                    </td>
                    <td>
                        @if ($bn->refunded)
                        <span class="badge badge-success">remboursé</span>
                        @else
                        <span class="badge badge-danger">Dette</span>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="justify-content-center">
            {{ $depts->links() }}
        </div>
    </div>
</div>
@endsection