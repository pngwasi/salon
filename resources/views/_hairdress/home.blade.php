@extends('hairdress')

@section('hairdress')
<div id="chart-home">
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Bonus ({{ now()->format('F') }})</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">${{ $bonus }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Salaire - 40% ({{ now()->format('F') }})
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">${{ $salary }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Coiffures ({{ now()->format('F') }})</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">${{ $hairdress }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Dette</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">${{ $depts }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Coiffure</div>
                <div class="card-body">
                    <canvas id="heardress" style="display: block; width: 100%; height: 300px;"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Bonus</div>
                <div class="card-body">
                    <canvas id="bonus" style="display: block; width: 100%; height: 300px;"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    window.chart_hairdress = @json($chart_hairdress);
    window.chart_bonus = @json($chart_bonus);
</script>
@endsection
