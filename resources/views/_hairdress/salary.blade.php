@extends('hairdress')

@section('hairdress')
<div class="card">
    <div class="card-header">
        <form method="GET" action="" autocomplete="off">
            <div class="form-row">
                <div class="col-lg-4 col-sm-12 col-sm-12 mb-3">
                    <input type="number" class="form-control" name="month" value="{{ request('month') }}"
                        placeholder="Mois (optionnel)">
                </div>
                <div class="col-lg-4 col-sm-12 col-sm-12 mb-3">
                    <input type="number" class="form-control" value="{{ request('year') }}" name="year"
                        placeholder="Year">
                </div>
                <div class="col-lg-4 col-sm-12 col-sm-12 mb-3">
                    <button type="submit" class="btn btn-primary">recherche</button>
                </div>
            </div>
        </form>
    </div>
    <div class="card-body">
        <table class="table m-0 text-muted">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Montant</th>
                    <th scope="col">Mois</th>
                    <th scope="col">Attente</th>
                    <th scope="col">Montant Entré</th>
                    <th scope="col">Année</th>
                    <th scope="col">Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($salaries as $bn)
                <tr>
                    <td>{{ $bn['id'] }}</td>
                    <td><span class="badge badge-info text-white">${{ $bn['amount'] }}</span></td>
                    <td>{{ $bn['month'] }}</td>
                    <td>${{ $bn['expected'] }} (40%)</td>
                    <td>${{ $bn['entry_amount'] }}</td>
                    <td>{{ $bn['year'] }}</td>
                    <td>
                        @if ($bn['salaryStatus'] > 0)
                        <span class="badge badge-success">Apprové</span>
                        @else
                        <span class="badge badge-secondary">No Apprové</span>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
