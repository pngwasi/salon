@extends('layouts.home-app')

@section('home-content')
<div data-controller="User-Hairdress">
    <h4>Coiffures</h4>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::url() == route('hairdress') ? 'active' : '' }}"  href="{{ route('hairdress') }}">{{ __('Acceuil') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::url() == route('hairdress.bonus') ? 'active' : '' }}"  href="{{ route('hairdress.bonus') }}">{{ __('Bonus') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::url() == route('hairdress.salary') ? 'active' : '' }}"  href="{{ route('hairdress.salary') }}">{{ __('Salaire') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::url() == route('hairdress.dept') ? 'active' : '' }}"  href="{{ route('hairdress.dept') }}">{{ __('Dette') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Request::url() == route('hairdress.hairdressing') ? 'active' : '' }}"  href="{{ route('hairdress.hairdressing') }}">{{ __('coiffure') }}</a>
                    </li>
                </ul>
                <div class="content mt-5">
                    @yield('hairdress')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
