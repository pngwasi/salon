@extends('layouts.home-app')

@section('home-content')
<div data-controller="User-Collect">
    <h4>Encaissement</h4>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs justify-content-center" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link {{ Request::url() == route('collect') ? 'active' : '' }}"  href="{{ route('collect') }}">{{ __('Acceuil') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Str::contains(Request::url(), route('collect.entries')) ? 'active' : '' }}"  href="{{ route('collect.entries') }}">{{ __('Entrés') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Str::contains(Request::url(), route('collect.bonus')) ? 'active' : '' }}"  href="{{ route('collect.bonus') }}">{{ __('Bonus') }}</a>
                    </li>
                </ul>
                <div class="content mt-3">
                    @yield('collect')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

